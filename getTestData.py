# one-time getting a list of clue, wordlength, and answer from database
# used to get test data for the solver tester

import psycopg2

def connect():
	"""Sets up connection to the testset database."""
	#Define our connection string
	conn_string = "host='localhost' dbname='testset' user='angrywords' password='dln'"
 
	# get a connection, if a connect cannot be made an exception will be raised here
	conn = psycopg2.connect(conn_string)
	return conn

def select(query, conn):
	"""Executes a query in the database, returns results."""
	cursor = conn.cursor()

	# execute our Query
	cursor.execute(query)
 
	# retrieve the records from the database
	return cursor.fetchall()

def main():
	connection = connect()
	query = "SELECT * FROM pairs"
	queryresult = select(query, connection)
	print queryresult
	outfile = open("testdata_large.txt", 'a')
	for item in range(3048):
		outfile.write(queryresult[item][0] + '|' + queryresult[item][1] + "|" + str(len(queryresult[item][1])) + "\n")
	outfile.close()
main()