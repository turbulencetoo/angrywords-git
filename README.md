# Angry Words

A crossword-solving AI

## Resources

- [Wiki](https://bitbucket.org/Kolvia/angrywords/wiki/Home) - All sorts of info about the project
- [Puz.py](https://github.com/alexdej/puzpy) - Python code to parse .puz files and return Puzzle objects
