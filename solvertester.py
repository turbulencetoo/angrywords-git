# solvertester.py
# Version: 10/18/12
import imp
import os
from time import time
from DataStructure.dataStructure import Answers

def getSolvers():
	"""Adds all solvers to our list of solvers using imp library"""
	modules = []
	synonym = imp.load_source("synonym_wn", "Solvers/Synonym/synonym.py")
	modules.append(synonym.Synonym())
	
	pastCrosswords = imp.load_source("pastCrosswords", "Solvers/PastCrosswords/pastCrosswords.py")
	modules.append(pastCrosswords.PastCrosswords()) 
	
	similarClues = imp.load_source("similarClues", "Solvers/SimilarClues/similarClues.py")
	modules.append(similarClues.SimilarClues())
	
	allPastAnswers = imp.load_source("allPastAnswers", "Solvers/AllPastAnswers/allPastAnswers.py")
	modules.append(allPastAnswers.AllPastAnswers())
	
	directions = imp.load_source("directions", "Solvers/Directions/directions.py")
	modules.append(directions.Directions())
	
	fillInTheBlank = imp.load_source("fillInTheBlank", "Solvers/FillInTheBlank/fillInTheBlank.py")
	modules.append(fillInTheBlank.FillInTheBlank())
	
	actor = imp.load_source("actor", "Solvers/Actor/actor.py")
	modules.append(actor.Actor())
	
	movieByYear = imp.load_source("movieByYear", "Solvers/MovieByYear/movieByYear.py")
	modules.append(movieByYear.MovieByYear())
	
	movieCharactersByActor = imp.load_source("movieCharactersByActor", "Solvers/MovieCharactersByActor/movieCharactersByActor.py")
	modules.append(movieCharactersByActor.MovieCharactersByActor())
	
	movieCharactersByCharacter = imp.load_source("movieCharactersByCharacter", "Solvers/MovieCharactersByCharacter/movieCharactersByCharacter.py")
	modules.append(movieCharactersByCharacter.MovieCharactersByCharacter())
	
	director = imp.load_source("director", "Solvers/Director/director.py")
	modules.append(director.Director())
	
	author = imp.load_source("author", "Solvers/Author/author.py")
	modules.append(author.Author())
	
	basicSphinx = imp.load_source("basicSphinx", "Solvers/BasicSphinx/basicSphinx.py")
	modules.append(basicSphinx.BasicSphinx())
	
	bookTitles = imp.load_source("bookTitles", "Solvers/BookTitles/bookTitles.py")
	modules.append(bookTitles.BookTitles())
	
	music = imp.load_source("music", "Solvers/Music/music.py")
	modules.append(music.Music())
	
	sportsPlayer = imp.load_source("sportsPlayer", "Solvers/SportsPlayer/sportsPlayer.py")
	modules.append(sportsPlayer.SportsPlayer())
	
	sphinxPhrase = imp.load_source("sphinxPhrase", "Solvers/SphinxPhrase/sphinxPhrase.py")
	modules.append(sphinxPhrase.SphinxPhrase())
	
	wiktionary = imp.load_source("wiktionary", "Solvers/Wiktionary/wiktionary.py")
	modules.append(wiktionary.Wiktionary())
	
	yagoLinks = imp.load_source("yagoLinks", "Solvers/YagoLinks/yagoLinks.py")
	modules.append(yagoLinks.YagoLinks())
	
	yagoFacts = imp.load_source("yagoFacts", "Solvers/YagoFacts/yagoFacts.py")
	modules.append(yagoFacts.YagoFacts())
	
	yagoTypes = imp.load_source("yagoTypes", "Solvers/YagoTypes/yagoTypes.py")
	modules.append(yagoTypes.YagoTypes())

	return modules

def testSolvers(cluelist):
	"""
	This function does the actual running of the solvers to see what they got correct.
	It also records all unsolved clues, and links solved clues to which modules solved them.
	It then writes that information to files.
	"""
	modules = getSolvers()
	modToCluesSolvedDict = {}
	for mod in modules:
		modToCluesSolvedDict[mod.__name__] = {}
	cluesToModDict = {}
	for i in range(len(cluelist)):
		cluesToModDict[i] = set([])
	allmoduleconfidences = []
	# test each module
	for module in modules:
		print module.__name__
		moduleconfidences = []
		for j in range(len(cluelist)):
			# try to answer a clue
			solution = module.solve(cluelist[j][0], int(cluelist[j][2]))
			answers = solution.getAnswers()
			# make sure there are answers and record if and where the correct answer is
			if answers:
				answersWOConfidences = [x[0].lower() for x in answers]
				if cluelist[j][1].lower() in answersWOConfidences:
					correctIndex = answersWOConfidences.index(cluelist[j][1].lower())
					modToCluesSolvedDict[module.__name__][j] = correctIndex
					cluesToModDict[j].add(module.__name__)
				else:
					correctIndex = -1
			else:
				correctIndex = -1
			moduleconfidences.append((solution.getConfidence(), correctIndex))
		allmoduleconfidences.append(moduleconfidences)
	
	# Make a file listing unanswered clues
	unsolvedClues = []
	for i in range(len(cluelist)):
		correctMods = cluesToModDict[i]
		if len(correctMods) == 0 or (len(correctMods) ==  1 and "allPastAnswers" in correctMods):
			unsolvedClues.append(cluelist[i][0] + " | " + cluelist[i][1])
	unsolvedCluesString = "\n".join(unsolvedClues)
	
	file = open("unsolved_clues.txt", 'w')
	file.write(unsolvedCluesString)
	file.close()

	#make a file listing who solved what
	stringList = []
	
	header = " " * 151
	for mod in modules:
		header += "%-8s" % mod.__name__[:7] 
	header += "\n"
	j=0
	for i in range(len(cluelist)):
		if j % 40 == 0:
			#print the header
			stringList.append(header)  
		stringList.append("%-135s %-15s" %(cluelist[i][0], cluelist[i][1]))
		for mod in modules:
			if i in modToCluesSolvedDict[mod.__name__]:
				stringList.append("%8d" % modToCluesSolvedDict[mod.__name__][i])
			else:
				stringList.append("		   ")
		stringList.append("\n")
		
		j+=1
		
	file = open("clueMatrix.txt", 'w')
	file.write("".join(stringList))
	file.close()

	# Make graphs
	makePlotFiles(allmoduleconfidences)
	plot(modules)
	for i in range(9):
		os.remove("module%d"%(2**i))
	  
def makePlotFiles(confidences):
	"""
	This function writes the data files need to build graphs.
	The graphs are of the success of each solver at a variety of depths in their answer lists.
	"""
	# look for the correct answer in the top k most confident answers
	testValues = [1, 2, 4, 8, 16, 32, 64, 128, 256] # k
	for i in range(len(confidences)):
	  confidences[i].sort(lambda x,y: cmp(x[0], y[0]), reverse=True)
	for value in testValues:
	  file = open("module%s"%value, 'w')
	  linelist = []
	  for i in range(len(confidences[0])):
		 linelist.append(str(i+1))
	  for module in confidences:
		 correct = 0
		 for i in range(len(module)):
			if module[i][1] < value and module[i][1]>-1:
				correct += 1 
			percent = correct/float(i+1)
			linelist[i] += "\t\t%f"%(percent)
	  filestring = "\n".join(linelist)
	  file.write(filestring)
	  file.close()

def plot(modules):
	"""This function uses gnuplot to actually write the graphs based on the data files."""
	file = open("temp.gnu", "w")
	filestring = ""
	for j in range(9):
		
		filestring += "set term svg\n"
		filestring += "set output \"test%d.svg\"\nplot "%(2**j)
		for i in range(len(modules)):		 
			filestring += "\"module%d\" using 1:%d title '%s' with lines, "%(2**j, i+2, modules[i].__name__)
		filestring = filestring[:-2]
		filestring += "\n"
	file.write(filestring)
	file.close()
	# make a system call to gnuplot to do the actual plotting
	os.system("cat temp.gnu | gnuplot")
	os.remove("temp.gnu")	 
	

def main():
	start_time = time()
	cluedata = open("testdata_large.txt")
	cluelist = []
	# counter used to select which pieces of test data to actually test on
	counter = 0
	for line in cluedata:
		if counter < 1000:
			counter += 1
			continue
		clue = line.split("|")
		cluelist.append(clue)
		counter += 1
		if counter == 1002:
			break
	testSolvers(cluelist)
	final_time = time()-start_time
	final_time = final_time/3600.
	file = open('unsolved_clues.txt', 'a')
	file.write("\nRunning Time: %.2f hrs Hello!" %final_time)
	file.close()
if __name__ == "__main__":
	main()
