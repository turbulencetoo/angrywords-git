"""
This code is just to build the bigraph and unigraph probabilities from the resolver database.
"""

from collections import *
import psycopg2
import re

class Probability:

	def __init__(self):
		self.bigramdict = defaultdict(float)
		self.unigramdict = defaultdict(float)

	def connect(self):
		"""Connect method sets up a connection to the resolver database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='resolver' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psycopg2.connect(conn_string)
	
		
	def select(self, query):
		"""Takes in query as parameter, executes query, and returns results."""
		cursor = self.conn.cursor()
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		return cursor.fetchall()
		
	def getProbability(self):
		"""This method gets all the probabilities and writes them to files."""
		query = "SELECT * FROM answers"
		self.connect()
		allAnswers = self.select(query)
		
		for string in allAnswers:
			letters = string[0]
			letters = letters + '^'
			self.bigramdict[('^' + letters[0])] += 1.0
			
			ascii = True
			for i in range(len(letters)):
				# manually eliminate unicode -- we couldn't find a better way, sadly
				if letters[i] not in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '^']:
					ascii = False
				if ascii:	  
					if i > 0:
						self.bigramdict[(letters[i-1] + letters[i])] += 1.0
						
					self.unigramdict[letters[i]] += 1.0
					if i == 0:
						self.bigramdict['^COUNT'] += 1
					else:
						self.bigramdict['%sCOUNT'%letters[i-1]] += 1
					self.unigramdict['COUNT'] += 1
					
		for ascii1 in range(26):
			for ascii2 in range(26):
				if chr(ascii1+65)+chr(ascii2+65) not in self.bigramdict:
					self.bigramdict[chr(ascii1+65)+chr(ascii2+65)] = 1.0
					print self.bigramdict[chr(ascii1+65)+chr(ascii2+65)], chr(ascii1+65)+chr(ascii2+65)
					self.bigramdict[chr(ascii1+65)+'COUNT'] += 1.0
			if chr(ascii1+65)+'^' not in self.bigramdict:
				self.bigramdict[chr(ascii1+65)+'^'] = 1.0
				self.bigramdict[chr(ascii1+65)+'COUNT'] += 1.0
			if '^'+chr(ascii1+65) not in self.bigramdict:
				self.bigramdict['^'+chr(ascii1+65)] = 1.0
				self.bigramdict['^'+'COUNT'] += 1.0
		
		#getting percentages for bigraphs
		for key in self.bigramdict.keys():
			if 'COUNT' not in key:
				self.bigramdict[key] = self.bigramdict[key]/self.bigramdict['%sCOUNT'%key[0]]
			
		# getting percentages for unigraphs	  
		for key in self.unigramdict.keys():
			if key != 'COUNT':
				self.unigramdict[key] = self.unigramdict[key]/self.unigramdict['COUNT']

		# writing bigraph file
		bifile = open('bigraphs.txt', 'w')
		for key in self.bigramdict.keys():
			if 'COUNT' not in key:
				line = key[1] + "\t" + key[0] + "\t" + str(self.bigramdict[key]) + '\n'
				bifile.write(line)
		
		# writing unigraph file
		unifile = open('unigraphs.txt', 'w')
		for key in self.unigramdict.keys():
			if key != 'COUNT':
				line = key + "\t" + str(self.unigramdict[key]) + '\n'
				unifile.write(line)

def main():
	prob = Probability()
	prob.getProbability()
	
if __name__ == '__main__':
	main()
	
			
	
		