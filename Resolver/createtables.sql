-- CREATE TABLE answers
-- (answer varchar(30),
-- freq integer,
-- 
-- PRIMARY KEY (answer)
-- );

CREATE TABLE bigraphprob
(cur char(1),
given char(1),
prob float,

PRIMARY KEY (cur, given)
);

-- CREATE TABLE unigraphprob
-- (graph char(1),
-- prob float,
-- 
-- PRIMARY KEY (graph)
-- );