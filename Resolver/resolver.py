'''
The goal of the resolver is to take a nearly completed puzzle, one with just a few blanks left,
and fill in the final blanks.  It does so using a variation on a Hidden Markov Model, although it might
more accurately be called a Markov Model, as none of the states are exactly hidden.	 The variant is
two dimensional, which was an additional challenge.	 Initially, it was set up to be optimal.  However,
in order to actually have the resolver run using the limited memory of the machine, and run in a
reasonable amount of time, we eventually made the solution non-optimal rather than simply limiting
which types of puzzles it could run on.

The resolver does not work within the parameters of the data structure.
Instead, it extracts information from the data structure and puts it into an n x n matrix.
Then, it runs the algorithm on said matrix, and puts the resultant data back into the data structure.
'''

from math import *
from heapq import *
import psycopg2
from DataStructure.dataStructure import *
from collections import *
#from Decider.decider import *

class Resolver:

	def __init__(self, puzzle):
		self.puzzle = puzzle.clone()
		self.matrix = [['^'] for i in range(self.puzzle.width + 1)] # row of black squares on either edge
		self.matrix[0] = ['^' for i in range(self.puzzle.height + 1)]
		# simple test puzzle for debugging
		#self.matrix = [['^', '^', '^', '^', '^'],['^', '^', 'R', '', 'A'],['^', 'R', '', 'I', 'L'],['^', 'A', 'R', '', 'S'],['^', 'N', 'E', 'E', '^']]
		self.populateMatrix()
		self.width = self.puzzle.width + 1
		self.height = self.puzzle.height + 1
		self.tooManyBlanksFlag = False
		self.findBlanks()
		self.bigramDict = defaultdict(lambda : defaultdict(float))
		self.heapSize = 0
		
		self.connect()
		results = self.select('SELECT * FROM bigraphprob')
		for result in results:
			self.bigramDict[result[1]][result[0]] = result[2]
		
	def populateMatrix(self):
		"""
		Builds up matrix from data structure, putting it into resolver format.
		Resolver has columns and rows reversed, uses different symbols for blanks,
		black spaces, etc., and has a row and column of black spaces at the leading edges.
		"""
		for rowIndex in range(len(self.puzzle.fill)):
			for colIndex in range(len(self.puzzle.fill[rowIndex])):
				self.matrix[colIndex + 1].append(self.puzzle.fill[rowIndex][colIndex])
				if self.matrix[colIndex + 1][rowIndex + 1] == ".":
					self.matrix[colIndex + 1][rowIndex + 1] = "^"
				if self.matrix[colIndex + 1][rowIndex + 1] == "-":
					self.matrix[colIndex + 1][rowIndex + 1] = ""
	
	def connect(self):
		"""Connect method sets up a connection to the resolver database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='resolver' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psycopg2.connect(conn_string)
		
	def select(self, query):
		"""Takes in query as parameter, executes query, and returns results."""
		cursor = self.conn.cursor()
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		return cursor.fetchall()
		
	def findBlanks(self):
		"""
		Creates a matrix of the same size as self.matrix that has a 0 where there are letters
		in self.matrix and a 1 where there are blanks.
		"""
		self.flagMatrix = [[] for i in range(len(self.matrix))]
		for i in range(len(self.matrix)):
			blankCount = 0
			for j in range(len(self.matrix[i])):
				if self.matrix[i][j]:
					self.flagMatrix[i].append(0)
				else:
					self.flagMatrix[i].append(1)
					blankCount += 1
			# if we see any columns with three or more blanks, we need to use a heap
			# this might decrease performance a bit, but cutting off at the 10000
			# best columns drastically increases it, so it's worth it overall
			if blankCount > 2:
				self.tooManyBlanksFlag = True
		
	def allColumns(self, columnIndex):
		"""
		Main column builder methods.  Calls recursive column builder method inside.
		Returns a list of all possible columns, maximum 10000 possible.
		"""
		# to prevent underflow, all probabilities are logs, so 0 means a probability
		# of 1
		
		# python native heap class has no size variable, so we keep track
		self.heapSize = 0
		column = self.matrix[columnIndex]
		returnList = []		
		for letter in range(len(column)):
			# Probability is 1, so don't change columnProb
			if not column[letter]:
				# when the first blank is seen, add in each letter of the alphabet
				# and call allColumnHelper on each of those 26 new columns
				for i in range(26):
					columnProb = 0
					columnCopy = column[:] # deep copy
					# add the probability of transitioning to the added letter from
					# the previous know letter
					columnProb += log(self.bigramDict[column[letter-1]][chr(i+65)])
					columnCopy[letter] = chr(i+65)
					# call allColumnHelper to find the next blank
					self.allColumnHelper(columnCopy, letter, columnProb, returnList)
				break
		if not returnList:
			returnList.append([column, 0])
			return returnList
		if self.tooManyBlanksFlag:
			# get rid of heap wrappers
			for i in range(len(returnList)):
				returnList[i] = returnList[i].item
		return returnList
		
	def allColumnHelper(self, column, index, startColumnProb, returnList):
		"""
		Recursive helper function for allColumns.
		"""
		# If at the end of the column, add it to the list/heap of possible columns
		if index == len(column)-1:
			if self.tooManyBlanksFlag:
				curItem = HeapWrapper((column, startColumnProb))
				if self.heapSize == 10000:
					otherItem = heappop(returnList)
					if curItem > otherItem:
						heappush(returnList, curItem)
					else:
						heappush(returnList, otherItem)
				else:
					self.heapSize += 1
					heappush(returnList, curItem)		
			else:
				returnList.append((column, startColumnProb))
			return
		# if coming out of an added letter, include the probability of transitioning
		# away from that letter to the next known letter.
		if column[index+1]:
			startColumnProb += log(self.bigramDict[column[index]][column[index+1]])
		# look for the next blank
		for letter in range(index+1, len(column)):
			# if you the end of column is reached before finding another blank,
			# add that column and return
			if (letter == len(column)-1 and column[letter]):
				if self.tooManyBlanksFlag:
					curItem = HeapWrapper((column, startColumnProb))
					if self.heapSize == 10000:
						otherItem = heappop(returnList)
						if curItem > otherItem:
							heappush(returnList, curItem)
						else:
							heappush(returnList, otherItem)
					else:
						heappush(returnList, curItem)
						self.heapSize += 1
				else:
					returnList.append((column, startColumnProb))
				return
			if not column[letter]:
				for i in range(26):
					columnProb = startColumnProb
					columnCopy = column[:] # deep copy
					columnProb += log(self.bigramDict[column[letter-1]][chr(i+65)])
					columnCopy[letter] = chr(i+65)
					# recursive call to allColumnHelper to find the next blank.
					self.allColumnHelper(columnCopy, letter, columnProb, returnList)
				break
		
	def bestPuzzle(self):
		"""
		This method finds the best puzzle by building the probability matrix using 
		allColumns and the probability dictionaries.  It then reconstructs the result 
		using _getResult and puts the answer back in the puzzle using _flipAnswer.
		"""
		self.connect()
		# remember, to prevent underflow, were using logs of probabilities, so 0 means
		# a probability of 1
		
		# make the empty probability matrix - the first column has probability of 1
		probMatrix = [[] for i in range(self.width)]
		probMatrix[0] = [[self.matrix[0], 0, None]]
		
		# loop through all the columns is the puzzle
		for i in range(len(self.matrix)-1):
			comparisons = 0
			# generate all the orderings on the next column
			nextColumns = self.allColumns(i+1)
			# loop through all those ordrings
			for j in range(len(nextColumns)):
				max = float('-inf')
				argMax = 0
				# figure out the maximum value path that leads to the current ordering
				for k in range(len(probMatrix[i])):
					comparisons += 1 
					transitionProb = 0
					for m in range(self.height):
						if self.flagMatrix[i][m] or self.flagMatrix[i+1][m]:
							transitionProb += log(self.bigramDict[probMatrix[i][k][0][m]][nextColumns[j][0][m]])
					transitionProb += nextColumns[j][1]
					pathProb = transitionProb + probMatrix[i][k][1]
					if pathProb > max:
						max = pathProb
						argMax = k
				# add max path to the probability matrix
				probMatrix[i+1].append([nextColumns[j][0], max, argMax])
		# extract the best series of columns from the probability matrix
		matrix = self._getResult(probMatrix)
		return self._flipAnswer(matrix)
					
	def _getResult(self, probMatrix):
		"""
		This method reconstructs the best puzzle result using the probability matrix.
		"""
		answerMatrix = [[] for i in range(self.width - 1)]
		curIndex = 0 
		cur = max(probMatrix[-1], key = lambda m: m[1])
		while cur[2] != None:
			answerMatrix[curIndex] = cur[0]
			curIndex += 1
			cur = probMatrix[self.width - curIndex - 1][cur[2]]
		answerMatrix.reverse()	
		return answerMatrix
		
	def _flipAnswer(self, matrix):
		"""
		This method reverses the changes to formatting made to put the puzzle in the resolver.
		It puts the matrix back into the puzzle, but with the resolver results.
		"""
		for colIndex in range(len(matrix)):
			for rowIndex in range(1, len(matrix[colIndex])):
				self.puzzle.fill[rowIndex-1][colIndex] = matrix[colIndex][rowIndex]
				if self.puzzle.fill[rowIndex-1][colIndex] == "^":
					self.puzzle.fill[rowIndex-1][colIndex] = "."
		return self.puzzle
		
class HeapWrapper:
	"""
	HeapWrapper used so that we can have a min heap even though it's built out of tuples.
	"""
	def __init__(self, item): self.item = item
	def __cmp__(self, other): return cmp(self.item[1], other.item[1])
		
def main():
	"""Test code"""
	puzzle = AWPuzzle("TestSet/ut070115.puz")
	puzzle.printSolution()
	puzzle.fill = puzzle.solution
	re = Resolver(puzzle)
	re.bestPuzzle()

if __name__ == '__main__':
	main()