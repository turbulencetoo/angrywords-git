#music.py
from DataStructure.dataStructure import Answers
import sqlite3
import re


class Music:
    def __init__(self):
        self.__name__ = "music"
        self.conn = sqlite3.connect('/crosswordcomps/track_metadata.db')
        self.cursor = self.conn.cursor()
        
    def identifyArtistNames(self, clue):
        words = clue.split()
        options = []
        curOption = []
        for i in range(len(words)):
            if words[i].istitle():
                curOption.append(words[i])
            else:
                if len(curOption) > 0:
                    options.append(" ".join(curOption))
                    curOption = []
        if len(curOption) > 0:
            options.append(" ".join(curOption))
        return options            
        
        
    def identifySongNames(self, clue):
        regex = "^.*[\"'](.*)[\"'].*$"
        matchOb = re.match(regex, clue)
        if matchOb:
            return matchOb.group(1)
        else:
            return None
        
    def identifyYear(self, clue):
        regex = "^.*([12][0-9][0-9][0-9]).*$"
        matchOb = re.match(regex, clue)
        if matchOb:
            return matchOb.group(1)
        else:
            return None
        
    def hasSongKeyWord(self, clue):
        clueSet = set(clue.lower().split())
        keyWords = set(["hit", "tune", "song", "number"])
        return len(clueSet & keyWords) > 0
        
    def hasArtistKeyWord(self, clue):
        clueSet = set(clue.lower().split())
        keyWords = set(["artist", "performer", "performed", "writer", "singer", "band"])
        return len(clueSet & keyWords) > 0
    
    def sanitizeSongTitleInput(self, originalTitle):
        options = [originalTitle, originalTitle.lower(), originalTitle.title()]
        smallWords = set(["a", "an", "the", "to", "as", "of", "by", "for"])
        options.append(" ".join([word.lower() if word in smallWords else word.title() for word in originalTitle.split()]))
        return options
    
    def sanitizeArtistInput(self, originalArtists):
        options = []
        smallWords = set(["a", "an", "the", "to", "as", "of", "by", "for"])
        for originalArtist in  originalArtists:
            options.extend([originalArtist, originalArtist.lower(), originalArtist.title()])
            options.append(" ".join([word.lower() if word in smallWords else word.title() for word in originalArtist.split()]))
        return options
    
    def sanitizeArtistOutput(self, artist):
        artist = artist.upper()
        artist = re.sub("[ !@#$%^&*()[\]\\_'\"-]", "", artist)
        return artist
    
    def sanitizeSongTitleOutput(self, songTitle):
        songTitle = songTitle.upper()
        songTitle = re.sub("[ !@#$%^&*()[\]\\_'\"-]", "", songTitle)
        return songTitle
    
    def solveForArtist(self, clue, length, songTitle):
        possibleAnswers = set([])
        titles = self.sanitizeSongTitleInput(songTitle)
        for title in titles:
            self.cursor.execute("SELECT DISTINCT artist_name FROM songs WHERE title = (?)", (title, ))
            artists = self.cursor.fetchall()
            artists = [tuple[0] for tuple in artists]
            for artist in artists:
                sanitizedArtistAnswer = self.sanitizeArtistOutput(artist)
                if len(sanitizedArtistAnswer) == length:
                    possibleAnswers.add(sanitizedArtistAnswer)
        if self.hasArtistKeyWord(clue):
            modConf = .75
        else:
            modConf = .05
        conf = 1.0
        if len(possibleAnswers) > 0:
            conf = conf/len(possibleAnswers)
        return Answers([(artist, conf) for artist in possibleAnswers], modConf)
    
    def solveForSong(self, clue, length):
        possibleAnswers = set([])
        possibleArtists = self.identifyArtistNames(clue)
        artists = self.sanitizeArtistInput(possibleArtists)
        for artist in artists:
            self.cursor.execute("SELECT DISTINCT title FROM songs WHERE artist_name = (?)", (artist, ))
            titles = self.cursor.fetchall()
            titles = [tuple[0] for tuple in titles]
            for title in titles:
                sanitizedTitleAnswer = self.sanitizeSongTitleOutput(title)
                if len(sanitizedTitleAnswer) == length:
                    possibleAnswers.add(sanitizedTitleAnswer)
        if self.hasSongKeyWord(clue):
            modConf = .75
        else:
            modConf = .05
        conf = 1.0
        if len(possibleAnswers) > 0:
            conf = conf/len(possibleAnswers)
        return Answers([(title, conf) for title in possibleAnswers], modConf)
        
    def solve(self, clue, length):
        import traceback
        try:
            possibleSongName = self.identifySongNames(clue)
            lookingForArtist = self.hasArtistKeyWord(clue)
            lookingForSong = self.hasSongKeyWord(clue)
            if possibleSongName and lookingForArtist:
                return self.solveForArtist(clue, length, possibleSongName)
            elif lookingForSong:
                return self.solveForSong(clue, length)
            elif possibleSongName:
                return self.solveForArtist(clue, length, possibleSongName)
            else:
                return self.solveForSong(clue, length)
        except:
            traceback.print_exc()
            return Answers([], 0)
    
    def test(self):
        print self.solve("Depeche Mode 1990 hit", 15)
        return self.solve("'Enjoy the Silence' artist", 11)
        
if __name__ == "__main__":
    m = Music()
    m.test()

    
