CREATE TABLE teams
(teamcode char(3),
 city char(50),
 team char(50),

PRIMARY KEY (teamcode)
);

CREATE TABLE players
(playercode char(20),
 firstname char(50),
 lastname char(50),
 team char(3)
);