"""
Sports module is just a basic format solver module.
Given a clue with a possible sports player answer, it looks up the player in the database and
returns all permutations of the last name that fit the cluelength.
"""

import re
import psycopg2 as psy
from DataStructure.dataStructure import Answers

class SportsPlayer:
	def __init__(self):
		self.__name__ = "sportsPlayer"
		
	def connect(self):
		"""Connect method sets up a connection to an sports database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='sports' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psy.connect(conn_string)

	def findTeams(self, clue):
		"""
		Parses clue looking for potential team names in the clue.
		Then looks up any possibilities in the database and returns their teamcodes.
		"""
		citiesOrTeams = re.findall(r'[A-Z0-9][a-z0-9]+', clue)
		cursor = self.conn.cursor()
		teamcodes = []
		for item in citiesOrTeams:
			query = "select teamcode from teams where city = '%s';" %item
			cursor.execute(query)
			result = cursor.fetchall()
			if result:
				teamcodes.append(result[0][0].strip())
			query = "select teamcode from teams where team = '%s';" %item
			cursor.execute(query)
			result = cursor.fetchall()
			if result:
				teamcodes.append(result[0][0].strip())
		return teamcodes
			
	def findPlayers(self, teams, clue):
		"""
		Given teamcodes and parts of names, looks up all players in the database.
		If any part of their name is in the clue, returns their names.
		"""
		players = re.findall(r'[A-Z][a-z]+', clue)
		cursor = self.conn.cursor()
		playerNames = set()
		for team in teams:
			for item in players:
				query = "select firstname, lastname from players where team = '%s' and firstname = '%s';" %(team, item)
				cursor.execute(query)
				result = cursor.fetchall()
				if result:
					for i in result:
						playerNames.add(i)
				query = "select firstname, lastname from players where team = '%s' and lastname = '%s';" %(team, item)
				cursor.execute(query)
				result = cursor.fetchall()
				if result:
					for i in result:
						playerNames.add(i)
		return playerNames

	def solve(self, clue, numSpaces):
		"""Main solve function.	 Gets teams, players, builds confidences."""
		self.connect()
		teams = self.findTeams(clue)
		playerNames = self.findPlayers(teams, clue)
		possibleAnswers = []
		for name in playerNames:
			firstname, lastname = name[0].strip(), name[1].strip()
			if len(firstname) == numSpaces and firstname not in clue:
				possibleAnswers.append(firstname)
			elif len(lastname) == numSpaces and lastname not in clue:
				possibleAnswers.append(lastname)
		confidences = []
		for answer in possibleAnswers:
			confidences.append((answer, 1.0/len(possibleAnswers)))
		if confidences:
			return Answers(confidences, .7)
		else:
			return Answers([], 0.0)
		
	def test(self):
		"""Test function containing test code."""
		self.connect()
		clue = "Smith of the Conquistadors"
		numSpaces = 4
		teams = self.findTeams(clue)
		playerNames = self.findPlayers(teams, clue)
		possibleAnswers = []
		for name in playerNames:
			firstname, lastname = name[0].strip(), name[1].strip()
			if len(firstname) == numSpaces and firstname not in clue:
				possibleAnswers.append(firstname)
			elif len(lastname) == numSpaces and lastname not in clue:
				possibleAnswers.append(lastname)
		print possibleAnswers
		confidences = []
		for answer in possibleAnswers:
			confidences.append((answer, 1.0/len(possibleAnswers)))
		if confidences:
			return Answers(confidences, .7)
		else:
			return Answers([], 0.0)