def parse(filename):
	file = open(filename)
	outstring = ''
	for line in file:
		line = line.replace("<", "")
		line = line.replace(">", "")
		line = line.replace("_", " ")
		line = line.split("\t")
		splitType = line[3].split(" ")
		if splitType[0] == 'wikicategory':
			line[3] = line[3][13:]
		elif splitType[0] == 'wordnet':
			line[3] = line[3][8:-10]
		else:
			continue
		if len(line[3]) > 200:
			continue
		outstring += line[1] + '\t' + line[3] + '\n'
	outfile = open('/crosswordComps/yagoTypes/yagoTypes.tsv', 'a')
	outfile.write(outstring)
	file.close()
	outfile.close()
	
def main():
	parse("/crosswordComps/yago2s_tsv/yagoTypes.tsv")

if __name__ == '__main__':
	main()
		