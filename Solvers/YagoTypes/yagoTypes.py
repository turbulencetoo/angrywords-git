from DataStructure.dataStructure import Answers 
from inflect import *
import psycopg2
import re

class YagoTypes:
	def __init__(self):
		self.__name__ = "yagoTypes"
		
	def connect(self):
		"""Connect method sets up a connection to the yago database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='yago' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psycopg2.connect(conn_string)

		
	def select(self, query):
		"""Takes in query as parameter, executes query, and returns results."""
		cursor = self.conn.cursor()
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		return cursor.fetchall()
		
	def parseClue(self, clue):
		"""Splits the clue and looks for proper nouns (words that start with capital letters).
		 Removes 's from words."""
		goodWords = []
		for word in clue.split():
			if word[-2:] == "'s":
				word = word[:-2]
			word = word.replace("'", "''")
			if word[0].istitle():
				goodWords.append(word.lower())
		return goodWords
		
	def solve(self, clue, length):
		""" Looks for database entries where the subject contains a proper noun in the	
		subject. Also looks for cases where those nouns are in the "type" field as well.
		"""
		self.connect()
		
		parsedClue = self.parseClue(clue)
		
		deplural = engine()
		
		results = []
		for c in parsedClue:
			query = "SELECT * FROM yagotypes WHERE lower(subject) LIKE '%{}%';".format(c)
			results.extend(self.select(query))
			query = "SELECT * FROM yagotypes WHERE lower(type) LIKE '%{}%';".format(c)
			results.extend(self.select(query))
		resultDict = {}
		for r in results:
			# look for proper answers in returned text
			splitSubject = re.sub("[,.!?\*:;\'\"()\-]", "", r[0])
			splitSubject = splitSubject.split(" ")
			for word in splitSubject:
				word = word.lower()
				if len(word) == length:
					# score answer 
					score = 1
					for c in parsedClue:
						if c in r[0] or c in r[1]:
							score += 1
					if word in resultDict:
						resultDict[word] += score
					else:
						resultDict[word] = score
						
			splitType = re.sub("[,.!?\*:;\'\"()\-]", "", r[1])
			splitType = splitType.split()
			# types are generally plural, so make them singular
			for pluralnoun in splitType:
				notplural = deplural.singular_noun(pluralnoun)
				if notplural:
					pluralnoun = notplural
			for word in splitType:
				word = word.lower()
				if len(word) == length:
					score = 1
					for c in parsedClue:
						if c in r[0] or c in r[1]:
							score += 1
					if word.lower in resultDict:
						resultDict[word] += score
					else:
						resultDict[word] = score
		#print resultDict
		total = 0.0
		max = 0
		for item in resultDict.keys():
			total += resultDict[item]
			if resultDict[item] > max:
				max = resultDict[item]
		returnList = []
		for item in resultDict.keys():
			returnList.append((item, resultDict[item]/total))
		if returnList:
			return Answers(returnList, max/total)
		else:
			return Answers([], 0)
	
	def test(self):
		"""Standard test method"""
		return self.solve(" Updike's Harry Angstrom", 6)
		
def main():
	yt = YagoTypes()
	yt.solve("D-Day's Dwight", len("EISENHOWER"))
	