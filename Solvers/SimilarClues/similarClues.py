"""
Uses cosine distance to find clues that are similar to the current one,
based on the knowledge that crossword clues tend to be reused or derivative

Written by Alex Kuntz and Veronica Lynn
"""

from sets import *
from ClueParser.clueParser import ClueParser
from Solvers.PastCrosswords.pastCrosswords import PastCrosswords
from DataStructure.dataStructure import Answers
import psycopg2

class SimilarClues:

    def __init__(self):
     self.clues = {}
     self.stemmedCluesByLength = {}
     self.__name__ = "similarClues"
     self.clueParser = ClueParser()
     
    def cosineDistance(self, clueA, clueB):
     """
     Computes the cosine distance between two clues, which are represented as sets
     """
     
     # Remove stopwords to reduce the number of matches
     stopwords = ["the", "of", "and", "to", "a", "in", "that", "is", "was", "he", "for", "it", "with", "as", "his", "on", "be", "at", "by"]
     for word in stopwords:
        if word in clueA:
            clueA.remove(word)
        if word in clueB:
            clueB.remove(word)
     
     cosdistance = float('inf')
     
     # Number of words in common
     numerator = len(clueA.intersection(clueB))
     
     if numerator != 0:
        # Square root of total words
        denominator = (len(clueA) * len(clueB)) ** (1/2.)
        
        # Divide number of words in common by the square root of total number of words
        if denominator != 0:
            cosdistance = numerator / denominator
         
     return cosdistance
     
    
    def getAllClues(self, wordLength):
        """
        Query the database for all clues that correspond to answers of a given length
        """
    
        # Check if we've queried db for this answer length before
        if wordLength in self.stemmedCluesByLength:
            return self.stemmedCluesByLength[wordLength]
        
        # Query database
        conn_string = "host='localhost' dbname='pastcrosswords' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        query = "SELECT stem, clue FROM stems WHERE wordlength = {};".format(wordLength)
        cursor.execute(query)
        clues = cursor.fetchall()

        conn.close()
     
        # Memoize results to speed up processing of future queries
        self.stemmedCluesByLength[wordLength] = clues
     
        return clues
    
    def closestClues(self, clue, wordLength):
        """
        Find all clues that have word(s) in common
        """
    
        allclues = self.getAllClues(wordLength)
        
        clueA = self.stem(clue)
        clueA = set(clueA)
        
        clueDistances = []   
        maxDistance = 0.0
        
        # Compute the cosine distance between stemmed versions of clues
        for stemmedClue, unstemmedClue in allclues:
            clueB = stemmedClue.split()
            clueB = set(clueB)
        
            distance = self.cosineDistance(clueA, clueB)
        
            if distance != float('inf'):
                clueDistances.append((distance, unstemmedClue))
                if distance > maxDistance:
                    maxDistance = distance
                    
        return clueDistances, maxDistance
        
        
    def stem(self, clue):
        """
        Stems the clue by removing punctuation and using the clue parser's stemming
        
        Returns the clue as a list of stems
        """
        
        clue = clue.lower()
        for punct in ['.','\'','"','?','(',')','!',',','&',';',':']:
            clue = clue.replace(punct, '')
        clue = self.clueParser.stem(clue)
        clueList = clue.split()
        return clueList
     
    def solve(self, clue, wordLength):
        try:
            if clue in self.clues:
                return self.clues[clue]
        
            # Find all similar clues and the distance of the closest clue
            similarClues, maxDistance = self.closestClues(clue, wordLength)
         
            helper = PastCrosswords()
            finalAnswers = []
            avgPastConf = 0.0
            if similarClues != []:
            
                # For each similar clue, find corresponding answers of the correct length and assign confidences
                for similarClue in similarClues:   
                    answers, pastConfidence = helper.solve(similarClue[1], wordLength)
                    avgPastConf += pastConfidence
                    for answer in answers:
                        goodWords = []
                        goodWords.append(answer[0])
                        newConf = answer[1]*similarClue[0]
                        
                        # Find words of the correct length that share the suggested answer's stem
                        destemmedWords = self.clueParser.destem(answer[0].lower())
                        for item in destemmedWords:
                            if len(item) == wordLength and item != answer[0].lower():
                                goodWords.append(item)
                        if goodWords:
                            for item in goodWords:
                                finalAnswers.append((item, newConf/(len(goodWords)+1)))

                avgPastConf = avgPastConf / len(similarClues)
                confidence = avgPastConf * maxDistance

                # Create a unique set of answers and their combined confidences            
                answers = {}
                sum = 0
                for answer in finalAnswers:
                    if answer[0] not in answers:
                        answers[answer[0]] = answer[1]
                    else:
                        answers[answer[0]] += answer[1]
                    sum += answer[1]
                
                # Assign final confidences to each answer
                finalFinalAnswers = []
                for answer in answers:
                    finalFinalAnswers.append((answer, answers[answer] / sum))
                
                self.clues[clue] = Answers(finalFinalAnswers, confidence)
                return self.clues[clue]
            else:
                self.clues[clue] = Answers([], 0.0)
                return self.clues[clue]
        except:
            return Answers([], 0.0)

    def test(self):
        return self.solve("Call of the Wild", len("ROAR"))
