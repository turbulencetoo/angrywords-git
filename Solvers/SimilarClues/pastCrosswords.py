import psycopg2
import sys
import math

class PastCrosswords:

    def __init__(self):
        self.clues = {}
     
    def connect(self):
        #Define our connection string
        conn_string = "host='localhost' dbname='pastcrosswords' user='angrywords' password='dln'"
     
        # get a connection, if a connect cannot be made an exception will be raised here
        self.conn = psycopg2.connect(conn_string)

        
    def select(self, query):
        cursor = self.conn.cursor()
    
        # execute our Query
        cursor.execute(query)
     
        # retrieve the records from the database
        return cursor.fetchall()

        
    def getAnswersForClue(self, clue, wordLength):
        cursor = self.conn.cursor()
    
        query = "SELECT * FROM pairs WHERE clue = %s AND LENGTH(answer) = %s;"
        cursor.execute(query, [clue, wordLength])
        
        return cursor.fetchall()
        
    def getTopAnswersForLength(self, wordLength):
        query = "SELECT * FROM answers WHERE LENGTH(answer) = {};".format(wordLength)
        
        records = self.select(query)
        
        query2 = "SELECT SUM(freq) FROM answers WHERE LENGTH(answer) = {};".format(wordLength)
        
        count = self.select(query2)
        
        return records, count
    
    
    def solve(self, clue, wordLength):
        if clue in self.clues:
            return self.clues[clue]
            
        self.connect()
        
        records = self.getAnswersForClue(clue, wordLength)
        
        if len(records) == 0:
            # Get most common words, decrease our solver confidence
            records, count = self.getTopAnswersForLength(wordLength)
            records.sort(lambda x,y: cmp(x[1], y[1]), reverse=True)
            
            best_guesses = []
            count = float(count[0][0])
            
            for answer in records:
                guess = (answer[0], answer[1]/count)
                best_guesses.append(guess)
                
            confidence = .1
            
            self.clues[clue] = (best_guesses, confidence)
        
        else:
            # Get frequency of the clue for answers of length wordLength
            freq_clue = 0.0
            for item in records:
                freq_clue += item[2]           
            
            # Get frequencies for all the words
            best_guesses = []
            
            for answer in records:
                guess = (answer[1], answer[2]/freq_clue)
                best_guesses.append(guess)
                
            best_guesses.sort(lambda x,y: cmp(x[1],y[1]), reverse=True)
            
            confidence = 1 + -1 * 1/freq_clue

            
            if confidence < .25:
                confidence = .25
            
            self.clues[clue] = (best_guesses, confidence)
            
            
        self.disconnect()
            
        return self.clues[clue]
        
    
    def disconnect(self):
        self.conn.close()
                
            


 
if __name__ == "__main__":
    solver = PastCrosswords()
    print solver.solve("';", 7)

