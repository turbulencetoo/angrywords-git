"""
This solver is one of the most direct ideas in puzzle solving--if we've seen a clue before,
and we know what the answer was then, there's a good chance that's what the answer is now.
It's based on clue-answer pair data from past crosswords.  This is one of the most common
strategies used by human crossword puzzle solvers too!	It only matches exact clues, although
it may match inexact answers, since it destems answers.	 A different solver, SimilarClues, matches
clues that are related rather than identical.
"""

import psycopg2
import math
import ClueParser.clueParser as clueParser
from DataStructure.dataStructure import Answers

class PastCrosswords:

	def __init__(self):
		self.clues = {}
		self.__name__ = "pastCrosswords"
	 
	def connect(self):
		"""Connect method sets up a connection to the pastcrosswords database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='pastcrosswords' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psycopg2.connect(conn_string)

		
	def select(self, query):
		"""Takes in query as parameter, executes query, and returns results."""
		cursor = self.conn.cursor()
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		return cursor.fetchall()

		
	def getAnswersForClue(self, clue):
		"""
		Builds query to return all answers for a clue, executes query, returns all answers.
		"""
		cursor = self.conn.cursor()
	
		query = "SELECT * FROM pairs WHERE clue = %s;"
		cursor.execute(query, [clue])
		
		return cursor.fetchall()
	
	
	def solve(self, clue, wordLength):
		"""
		Takes in clue, wordlength, gets all answers for given clue.
		If correct length, adds to guesses, then builds confidences based on guesses.
		Uses destemming to give more possibilities."""
		if clue in self.clues and len(self.clues[clue].getAnswers()[0][0]) == wordLength:
			return self.clues[clue]
			
		self.connect()
		destemmedStuff = []
		records = self.getAnswersForClue(clue)
		parser = clueParser.ClueParser()
		if len(records) == 0:		   
			return Answers([], 0.0)
		
		else:
			# Get frequency of the clue for answers of length wordLength
			freq_clue = 0.0
			
			# destemmed answers adding
			for item in records:
				if len(item[1]) == wordLength:
					freq_clue += item[2]*2
				destemmedAnswers = parser.destem(item[1])
				for word in destemmedAnswers:
					if len(word) == wordLength and word != item[1]:
						freq_clue += item[2]
						destemmedStuff.append((0, word, item[2]))
			
			# Get frequencies for all the words
			best_guesses = []
			if freq_clue == 0:
				self.disconnect()
				return	Answers([], 0.0)
			for answer in records:
				if len(answer[1]) == wordLength:
					guess = (answer[1], answer[2]*2/freq_clue)
					best_guesses.append(guess)
			for answer in destemmedStuff:
				guess = (answer[1], answer[2]/freq_clue)
				best_guesses.append(guess)
			best_guesses.sort(lambda x,y: cmp(x[1],y[1]), reverse=True)
			
			confidence = 1 + -1 * 1/freq_clue
			
			# threshold confidence
			if confidence < .25:
				confidence = .25
			
			self.clues[clue] = Answers(best_guesses, confidence)
			
			
		self.disconnect()
			
		return self.clues[clue]
		
	
	def disconnect(self):
		"""Closes connection."""
		self.conn.close()
		
	def test(self):
		"""Test code; enever executed except when testing."""
		newconn = psycopg2.connect("host='localhost' dbname='testset' user='angrywords' password='dln'")
		query = "SELECT * FROM pairs"
		cursor = newconn.cursor()
		tot = 0
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		queryresult =  cursor.fetchall()
		for i in range(2000):
			solution = self.solve(queryresult[i][0], len(queryresult[i][1]))
			print solution.getAnswers()
			print queryresult[i][1]
			if queryresult[i][1].lower() in	 [x[0].lower() for x in solution.getAnswers()]:
				tot += 1
		print tot
		return Answers([], 0)
