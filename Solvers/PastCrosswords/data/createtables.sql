CREATE TABLE clues
(clue varchar(1000),
freq integer,

PRIMARY KEY (clue)
);

CREATE TABLE answers
(answer varchar(50),
freq integer,

PRIMARY KEY (answer)
);

CREATE TABLE pairs
(clue varchar(1000),
answer varchar(50),
freq integer,

PRIMARY KEY (clue, answer),
FOREIGN KEY (clue) REFERENCES clues (clue),
FOREIGN KEY (answer) REFERENCES answers (answer)
);

CREATE TABLE stems
(clue varchar(1000),
stem varchar(1000),
wordlength integer,

FOREIGN KEY (clue) REFERENCES clues (clue)
);
