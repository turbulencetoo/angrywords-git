"""File used to parse pairs file and stem entries to put into stems file for SimilarClues solver.
Clearly, specialized to Ian's computer, because he was the one who did this most recently."""

from ClueParser.clueParser import *

clues = open("/Account/heinzmai/AngryWords/Solvers/PastCrosswords/pairs.txt")

cp = ClueParser()
outstring = ''

for clue in clues:
	clue = clue.split('\t')
	clueStem = cp.stem(clue[0])
	outstring += clue[0] + '\t' + clueStem + '\t' + str(len(clue[1])) + '\n'
clues.close()
outfile = open('/Account/heinzmai/AngryWords/Solvers/PastCrosswords/stems.txt', 'w')
outfile.write(outstring)
outfile.close()