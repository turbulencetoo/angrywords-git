"""
File used to parse .puz files and get them put into our PastCrosswords database.
"""

from puz import *

def printPuz(puzzle):
	"""Prints the puzzle"""	   
	for i in range(len(puzzle.solution)):
		print '%2s' %puzzle.solution[i],
		if i > 0:
			if (i+1)%puzzle.width == 0:
				print
			
def makePuzArray(puzzle):
	"""Given a puzzle object, returns a 2d array of solutions"""
	puzArray = []
	for i in range(puzzle.height):
		newrow = []
		for j in range(puzzle.width):
			newrow.append(puzzle.solution[(i*puzzle.width)+j])
		puzArray.append(newrow)
	return puzArray

def getClueAnswerPairs(puzzle, puzArray):
	"""Given a puzzle object and a puzzle array (from makePuzArray), returns 
	a list of (answer, clue) tuples"""
	clues = puzzle.clues
	clueAnswerPairs = []
	i = 0
	j = 0
	while len(clues) > 0:
		while j < puzzle.width and puzArray[i][j] == '.':
			j += 1
		if j >= puzzle.width:
			j = 0
			i += 1
		
		while j < puzzle.width and puzArray[i][j] == '.':
			j += 1
		curAcross = ''
		while j < puzzle.width and puzArray[i][j] != '.':
			 curAcross += puzArray[i][j]
			 j += 1
		if len(curAcross) > 1:
			cluepair = (curAcross, clues.pop(0))
			clueAnswerPairs.append(cluepair)
		m = j - len(curAcross)
		for k in range(len(curAcross)):
			n = i
			if (n-1) < 0 or puzArray[n-1][m] == ".":
				curDown = ''
				while n < puzzle.height and puzArray[n][m] != '.':
					curDown += puzArray[n][m]
					n += 1
				if len(curDown) > 1:
					cluepair = (curDown, clues.pop(0))
					clueAnswerPairs.append(cluepair)
			m += 1
			
	return clueAnswerPairs

def getPairsFromFile(fname):
	"""Given a .puz file, returns a list of (answer, clue) tuples"""
	if fname[-4:] == ".puz":
		print fname
		puzzle = read(fname)
		puzArray = makePuzArray(puzzle)
		return getClueAnswerPairs(puzzle, puzArray)

