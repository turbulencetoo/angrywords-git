# This file parses a directory of .puz files and outputs .txt files to be copied into psql
# tables (clues, answers, and pairs)


# To import into psql:
#	\copy clues from 'clues.txt';
#	\copy answers from 'answers.txt';
#	\copy pairs from 'pairs.txt';

import os
import codecs
from puzparser import getPairsFromFile

clues = {}
answers = {}
pairs = {}
# whatever the testset is, put the folder here
# the most recent test set is listed
test_set = os.listdir('/Accounts/heinzmai/AngryWords/TestSet/')
test_set.extend(os.listdir('/crosswordComps/comps-puzzles/TEST-2011/'))

for folder in os.listdir('/crosswordComps/2007puzzles_2/'):

	path = '/crosswordComps/2007puzzles_2/%s/'%folder
	
	with open('badfiles.txt', 'w') as bad_files:
		for f in os.listdir(path):
			# Make sure file is not in test set 
			if f in test_set:
				print "test set puzzle"
				continue
	
			# Make sure file is correct format; write filename to file if there's an error of some sort
			if f[-4:] == ".puz": 
				try:
					ca_pairs = getPairsFromFile(path+f)
				except:
					bad_files.write(path+f+"\n")
					continue
					
				if ca_pairs:
					for pair in ca_pairs:
						clue = pair[1].replace('\t', '')
						if len(clue) > 0:
							if clue[-1] == '\\':
								clue = clue[:-1]
							if clue[0] == '\\':
								clue = clue[1:]
						try:
							answer = unicode(pair[0], errors='replace')
						except:
							print repr(pair[0])
						
						# Get clue frequencies
						if clue in clues:
							clues[clue] += 1
						else:
							clues[clue] = 1
						
						# Get answer frequencies
						if answer in answers:
							answers[answer] += 1
						else:
							answers[answer] = 1
						
						# Get clue, answer pair frequencies
						if clue in pairs:
							if answer in pairs[clue]:
								pairs[clue][answer] += 1
							else:
								pairs[clue][answer] = 1
						else:
							pairs[clue] = {answer: 1}
							
				else:
					print "NO CLUE/ANSWER PAIRS:", path + f
					
for folder in os.listdir('/crosswordComps/comps-puzzles/TRAINING/'):

	path = '/crosswordComps/comps-puzzles/TRAINING/%s/'%folder
	
	with open('badfiles.txt', 'w') as bad_files:
		for f in os.listdir(path):
			# Make sure file is not in test set 
			if f in test_set:
				print "test set puzzle"
				continue

			# Make sure file is correct format; write filename to file if there's an error of some sort
			if f[-4:] == ".puz": 
				try:
					ca_pairs = getPairsFromFile(path+f)
				except:
					bad_files.write(path+f+"\n")
					continue
					
				if ca_pairs:
					for pair in ca_pairs:
						clue = pair[1].replace('\t', '')
						if len(clue) > 0:
							if clue[-1] == '\\':
								clue = clue[:-1]
							if clue[0] == '\\':
								clue = clue[1:]
						try:
							answer = unicode(pair[0], errors='replace')
						except:
							print repr(pair[0])
						
						# Get clue frequencies
						if clue in clues:
							clues[clue] += 1
						else:
							clues[clue] = 1
						
						# Get answer frequencies
						if answer in answers:
							answers[answer] += 1
						else:
							answers[answer] = 1
						
						# Get clue, answer pair frequencies
						if clue in pairs:
							if answer in pairs[clue]:
								pairs[clue][answer] += 1
							else:
								pairs[clue][answer] = 1
						else:
							pairs[clue] = {answer: 1}
							
				else:
					print "NO CLUE/ANSWER PAIRS:", path + f
			
		# Write text files to be copied into db, one per table (clues, answers, pairs)
with codecs.open('clues.txt', 'w', encoding='utf-8') as clue_file:
	for clue in clues:
		line = unicode("{}\t{}\n")
		clue_file.write(line.format(clue, clues[clue]))
	
with codecs.open('answers.txt', 'w', encoding='utf-8') as answer_file:
	for answer in answers:
		answer_file.write(line.format(answer, answers[answer]))
	
with codecs.open('pairs.txt', 'w', encoding='utf-8') as pair_file:
	for clue in pairs:
		for answer in pairs[clue]:
			line = unicode("{}\t{}\t{}\n")
			pair_file.write(line.format(clue, answer, pairs[clue][answer]))

