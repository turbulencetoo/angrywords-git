'''
synonym_wn.py

Donny Mellstrom
Ian Heinzman
Oct. 21, 2012

Uses WordNet (via NLTK) to gather synonymous lemmas
for all senses of the input word.

In addition it now searches "similar_to" and "also_see" pointers.

Also finds antonyms if first word of clue negates.

============================================================
Works Cited:

		WordNet. Princeton University. 2010. <http://wordnet.princeton.edu>
		License: http://wordnet.princeton.edu/wordnet/license/

		Python NLTK (with Numerical Python and PyYAML) - available from
		<http://www.nltk.org/>
============================================================

Usage Notes: Requires the above packages. Also requires separate installation
of WordNet, as well as the WordNet corpus on NLTK. After WN is installed,
import nltk at a Python prompt and enter the following:

		nltk.download('wordnet')

'''
import re
import unicodedata
import ClueParser.clueParser as cp
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords as sw
from DataStructure.dataStructure import Answers

class Synonym():

		def __init__(self):
				self.__name__ = "synonym"
		 
		def solve(self, clue, length):
				''' Tries to find synonyms of the entire clue. If it can't checkSeparate
				is called to find synonyms to individual words in the clue.'''
				
				#Detect whether to use antonym mode
				ant = False
				words = clue.split()
				if words[0] in ["not","Not","Don't","don't"]:
						ant = True
						clue = ""				#flag antonym and take off flagword
						for word in words[1:]:
								clue += word + " "
						clue = clue[:-1]		#take off extra space
						
						
				clue = re.sub("[?!.,-]", " ", clue) # Commas might be important in later versions (eg. "erase, in a way")
				clue = re.sub("\s+","_", clue)

				results = []
				#get wordnet data
				synsets = wn.synsets(clue)
				for synset in synsets:
						lemmaList = synset.lemmas
						for similar in synset.similar_tos():
								lemmaList.extend([lemma for lemma in similar.lemmas])
						for also_see in synset.also_sees():
								lemmaList.extend([lemma for lemma in also_see.lemmas])
						for lemma in lemmaList:
								if ant and len(lemma.antonyms()) > 0:
										for antonym in lemma.antonyms():
												results.append(antonym.name)
								elif not ant:
										results.append(lemma.name)
								
				#weed out duplicates and wrong length
				i = 0
				clueWOSpace = clue.replace("_", "").lower()
				while i < len(results):
						results[i] = self.flattenToAlpha(results[i])
						i += 1
				i = 0
				stemmer = cp.ClueParser()
				possible_answers = []
				while i < len(results):
					destemmed_list = stemmer.destem(results[i].lower())
					for word in destemmed_list:
						if stemmer.stem(word) != stemmer.stem(clueWOSpace):
							possible_answers.append(word)
					i += 1
				i = 0
				while i < len(possible_answers):
						if len(possible_answers[i]) != length or possible_answers[i] == clueWOSpace or (possible_answers[i] in possible_answers[0:i]):
								possible_answers.pop(i)
						else:
								i += 1
								
				# If there are no results and there are multiple words, check words individually
				if possible_answers:
						answers = self.getConfidences(possible_answers)
						return Answers(answers[0], answers[1])
				else:
						if "_" in clue:
							answers = self.checkSeparate(clue, length)
							# lower module confidence if we have split up the clue
							return Answers(answers[0], 0.4)
						else:
							return Answers(possible_answers, 0)
		
		def checkSeparate(self, clue, length):
				'''This method is used to look up individual words in multi-word clues that aren't in WordNet
				If this function is used, the solver will report a lower self-confidence.'''
		
				stopwords = sw.words('english')
				words = re.split("_", clue)
				results = []
				
				i = 0
				# Strip out stopwords (Should we do this????)
				while i < len(words):
						if words[i] in stopwords:
								words.pop(i)
						else:
								i += 1		  
				# Find synonyms for each word
				for word in words:
						synsets = wn.synsets(word)
						for synset in synsets:
								for lemma in synset.lemmas:
										results.append(lemma.name)
				
				# Build out list
				results = self.buildOut(results)
						
				# Get rid of redundant and wrong length
				i = 0
				clueWOSpace = clue.replace("_", "").upper()
				while i < len(results):
						results[i] = self.flattenToAlpha(results[i])
						i += 1
				i = 0
				while i < len(results):
						if len(results[i]) != length or results[i] == clueWOSpace or (results[i] in results[0:i] or results[i] in results[i+1:len(results)]):
								results.pop(i)
						else:
								i += 1
				if results:
						return self.getConfidences(results)
				else: 
						return [results, 0]

		def flattenToAlpha(self, unicodeInput):
				'''Strips all non-alpha characters from an input string (byte or Unicode), making it suitable as an answer'''
				#remove all accents
				if type(unicodeInput) == str:
						deaccented = unicodedata.normalize('NFKD', unicode(unicodeInput)).encode('ascii', 'ignore')
				else:
						deaccented = unicodedata.normalize('NFKD', unicodeInput).encode('ascii', 'ignore')
				#remove all non alphanum. and return in xword answer format
				flat = ''.join(e for e in deaccented if e.isalpha())
				return flat.upper()
				
		def buildOut(self, results):
				'''Builds out the list of synonyms by looking at synonyms of synonyms'''
				return results
		
		def getConfidences(self, results):
				'''Uses an algorithm to get the confidences, but for now just makes tuples
				weights everything equally.'''
				score = 1.0/len(results)
				for i in range(len(results)):
						results[i] = (results[i], score)
				return [results, 0.3]
				
		def test(self):
			return self.solve('melancholy', 3)