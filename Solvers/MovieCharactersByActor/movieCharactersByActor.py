"""
Uses the IMDB database to return character names given actor names and movie titles

Written by Alex Kuntz and Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools

class MovieCharactersByActor:
    def __init__(self):
        self.__name__ = "movieCharactersByActor"
    
    def getTitle(self, clue):
        '''
        Returns the part of the clue inside quotes (we assume this will be the title)
        '''
        match = re.search('"(.+?)"', clue)
        if match:
            return match.group(1), clue.replace('"' + match.group(1) + '"', '')
        else:
            return '', clue
            
    def getName(self, clue):
        '''
        Returns all capitalized words (we assume this will be actor names)
        '''
        names = []
        for word in clue.split():
            if word[0].istitle() and word != 'Actor' and word != "Actress":
                names.append(word)
        
        return names
        
    def getSubsets(self, things):
        """
        Compute all subsets of all permutations of a list. We do this so we can return
        first name, last name, or both
        """
    
        final = []
        for i in range(1,len(things) + 1):
            subsets = list(itertools.combinations(things, i))
            for subset in subsets:
                final += self.getPermutations(subset)
        
        return list(set(final))
    
        
    def getPermutations(self, names):
        '''
        Returns all permutations of possible names. We do this in case the actor has 
        a middle name or multiple middle names we can still find all combinations of 
        first name and last name.
        '''
        namesToSearch = []
        for item in itertools.permutations(names):
            for i in range(len(item)):
                firstName = item[i:]
                firstName = ' '.join(firstName)
                lastName = item[:i]
                lastName = ' '.join(lastName)
                namesToSearch.append((firstName, lastName))
            namesToSearch.append(('',' '.join(item)))
        return namesToSearch
        
    def lookupCharacterFromActorAndTitle(self, possibleNames, title):
        '''
        Given actor names and a title, returns found character names
        '''
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allNames = []
        
        for name in possibleNames:
            #If we only have a last name
            if name[0] == '':
                query = "SELECT char_name FROM actors WHERE lname = %s and (title = %s or title = %s);"
                cursor.execute(query, [name[1], title, '"'+title+'"'])
            #If we only have a first name
            elif name[1] == '':
                query = "SELECT char_name FROM actors WHERE fname = %s and (title = %s or title = %s);"
                cursor.execute(query, [name[0], title, '"'+title+'"'])
            #If we have first name and last name
            else:
                query = "SELECT char_name FROM actors WHERE fname = %s and lname = %s and (title = %s or title = %s);"
                cursor.execute(query, [name[0], name[1], title, '"'+title+'"'])
            charNames = cursor.fetchall()
            for charName in charNames:
                if charName[0] not in allNames:
                    allNames.append(charName[0])
        conn.close()
        return allNames
    
    

    def formatNames(self, names, answerLength):
        '''
        Removes all non letters from given names and ensures that all confidences sum to 1
        '''
        allNames = []
        finalNames = []
        for name in names:
            name = re.sub('[^a-zA-Z]', '', name)
            if len(name) == answerLength:
                allNames.append(name)
        for name in allNames:
            finalNames.append((name.lower(),1./len(allNames)))      
        return finalNames
    
    
    def solve(self, clue, ansLength):
    	'''
		Returns the best guesses for a clue and a given number of spaces
		'''
        try:
            title, strippedClue = self.getTitle(clue)
            #If we don't find a title, return empty
            if title:
                namesInClue = self.getName(strippedClue)
                selfConf = 1.0
                #If we don't find a name, return empty
                if namesInClue:
                    allPossibleNames = self.getSubsets(namesInClue)
                    names = self.lookupCharacterFromActorAndTitle(allPossibleNames, title)
                    #If we don't find any character names, return empty, otherwise format character names
                    if names:
                        answers = self.formatNames(names, ansLength)
                        return Answers(answers, selfConf)
                    else:
                        return Answers([], 0.0)
                else:
                    return Answers([], 0.0)
            else:
                return Answers([], 0.0)
        except:
            return Answers([], 0.0)
            
            
    def test(self):
        return self.solve('"Lost" role played by O\'Quinn', 9)
