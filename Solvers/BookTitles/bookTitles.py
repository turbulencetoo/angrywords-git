"""
Uses book database to find book titles based on authors' names

Written by Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools


class BookTitles:
    
    def __init__(self):
        self.__name__ = "bookTitles"
        
        
    def getNames(self, clue):
        """
        Get a list of all title case words in the clue in the hopes that it's a name
        """
        names = []
        for word in clue.split():
            if word[0].istitle():
                names.append(word.lower())
        return self.getPermutations(names)
    
    def getPermutations(self, names):
        """
	    Given a list of things that could be names, creates all permutations of first and last names
	    """
        namesToSearch = []
        for i in range(1,len(names) + 1):
            for item in itertools.permutations(names, i):
                namesToSearch.append(' '.join(item))
        return namesToSearch
    
    def lookupByAuthor(self, names):
        """
        Given a list of author names to try, queries the db for book titles
        """
        conn_string = "host='localhost' dbname='books' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allTitles = []
        for name in names:
            query = "SELECT title FROM books WHERE author = %s;"
            cursor.execute(query, [name])
            titles = cursor.fetchall()
            for title in titles:
                allTitles.append(title[0])
        conn.close()
        return allTitles 
    
    def getPossibleSolutions(self, titles, wordLength):
        """
        Given a list of titles returned by the query, find ones that fit the length requirement
        """
        finalTitles = []
        confTitles = []
        for title in titles:
            # If the title starts with "the" or "a", try it without
            if title[:4] == "the ":
                title2 = title[4:]
                title2 = re.findall("[a-z]+", title2)   #strip punctuation, spaces, etc.
                title2 = ''.join(title2)
                if len(title2) == answerLength:
                    finalTitles.append(title2)
            if title[:2] == "a ":
                title2 = title[2:]
                title2 = re.findall("[a-z]+", title2)
                title2 = ''.join(title2)
                if len(title2) == answerLength:
                    finalTitles.append(title2)
            
            title = re.findall("[a-z]+", title)
            title = ''.join(title)
            if len(title) == wordLength:
                finalTitles.append(title)
        # Assign confidence uniformly
        for title in finalTitles:
            confTitles.append((title, 1.0/len(finalTitles)))
    
        return confTitles
        
    def solve(self, clue, wordLength):
        try:
            names = self.getNames(clue)
            if names:
                titles = self.lookupByAuthor(names)
                if titles:
                    solutions = self.getPossibleSolutions(titles, wordLength)
                    
                    # Sets overall solver confidence
                    if "novel" in clue.lower() or "book" in clue.lower():
                        selfConf = 1.0
                    else:
                        selfConf = 0.2
                    return Answers(solutions, selfConf)
            return Answers([], 0.0)
        except:
            return Answers([], 0.0)
        
    def test(self):
        return self.solve("2012 Gillian Flynn novel", 8)   # Gone Girl
















