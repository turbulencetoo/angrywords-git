'''
This solver takes every word in a clue and looks for a Wikipedia page on that word. It then
looks at every link within those Wikipedia page. These links are the guesses that the solver supplies.
The guesses are given extra confidence if the Wikipedia article for the guess links back to the
original Wikipedia article

Written by Alex Kuntz
'''

import psycopg2
from DataStructure.dataStructure import Answers
from ClueParser.clueParser import ClueParser
import re
import itertools
import time
from operator import itemgetter

class YagoLinks:
    def __init__(self):
        self.cleanLinkRE = '(\[\[([A-Za-z]+?)\|([^\]]+?)\]\])'
        self.soloLink = '(\[\[([A-Za-z]+?)\]\])'
        self.wikiArticles = []
        self.stopwords = [word.strip().lower() for word in open('Solvers/stopwords.txt')]
        self.__name__ = "yagoLinks"
        
        
    def openPostgres(self):
        """Sets up and returns a connection."""
        conn_string = "host='localhost' dbname='yagolinks' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        return conn
        
    def findLinks(self,clue):
        '''
        Finds all Wikipedia articles that contains at least one similar noun from the given clue
        '''
        
        allLinks = []      
        conn = self.openPostgres()
        cursor = conn.cursor()
        for word in clue.split():
            if word.lower() not in self.stopwords:
                 word = word.lower()
                 word = word.replace("'","")
                 query = "SELECT link FROM yagolinks WHERE title = '<{}>';".format(word)
                 cursor.execute(query)
                 links = cursor.fetchall()
                 for link in links:
                    allLinks.append(link[0][1:-1])
        conn.close()
        return allLinks
    
    def checkBack(self, original, new):
        '''
        Checks to see if "original" is linked from "new"
        '''
        links = self.findLinks(new)
        if original in links:
            return True
        else:
            return False
                
    def formatAnswer(self, original, guess, numSpaces):
        '''
        Sets the confidence based on whether the guess links back to the original page
        Splits the guess at underscores
        '''
        allAnswers = []
        if self.checkBack(original, guess):
            conf = 2.0
        else:
            conf = 1.0
        guesses = guess.split("_")
        for guess in guesses:
            if len(guess) == numSpaces:
                allAnswers.append((guess, conf))
        return allAnswers
                                
                
    def test(self):
        return self.solve("City in Texas", 6)
        
    def solve(self, clue, numSpaces):
    	'''
		Returns the best guesses for a clue and a given number of spaces
		'''
        try:
            guessesDict = {}
            sum = 0.0
            searchTerms = clue.split()
            for term in searchTerms:    
                links = self.findLinks(clue)
                for link in links:
                    #Checks to see if "link" links back to "term"
                    guesses = self.formatAnswer(term, link, numSpaces)
                    #Concatinates identical guesses
                    for guess in guesses:
                        if guess[0] not in guessesDict:
                            guessesDict[guess[0]] = guess[1]
                        else:
                            guessesDict[guess[0]] += guess[1]
                        sum += guess[1]
            finalList = []
            #Ensures that confidences sum to 1
            for guess in guessesDict:
                finalList.append((guess, guessesDict[guess]/sum))
            finalList = sorted(finalList, key=itemgetter(1))
            finalList.reverse()
            
            #Self confidence caluclated based on length of the clue. Shorter clues
            #have higher confidence
            selfConf = 1.0/len(searchTerms)
            
            return Answers(finalList, selfConf)
        except:
            return Answers([],0.0)
