"""
Uses book database to find the name of authors based on the title of a book

Written by Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools


class Author:
    
    def __init__(self):
        self.__name__ = "author"
        
    def getTitle(self, clue):
        """
        If something is in quotes, assume it's a title
        """
        quotes = '"(.+?)"'
        search = re.search(quotes, clue)
        if search:
            return search.group(1).lower()
        return ''
    
    def lookupByTitle(self, title):
        """
        Given a possible title, see if we can find the author
        """
        conn_string = "host='localhost' dbname='books' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allAuthors = []
        possibleTitles = [title]
        
        # Try the title without "the" or "a" as well
        if title[:4] == "the ":
            possibleTitles.append(title[4:])
        if title[:2] == "a ":
            possibleTitles.append(title[2:])
        
        for title in possibleTitles:
            query = "SELECT author FROM books WHERE title = %s;"
            cursor.execute(query, [title])
            authors = cursor.fetchall()
            for author in authors:
                allAuthors.append(author[0].lower())
        conn.close()
        return allAuthors 
    
    def getPossibleSolutions(self, authors, wordLength):
        """
        Given a list of authors, try to find ones that could possibly be the answer
        """
        final = []
        finalConf = []
        for author in authors:
            names = author.split()
            for i in range(1,len(names) + 1):
                for item in itertools.combinations(names, i):
                    name = ''.join(item)
                    if len(name) == wordLength:
                        final.append(name)
        for name in final:
            finalConf.append((name, 1.0/len(final)))
    
        return finalConf
        
    def solve(self, clue, wordLength):
        try:
            title = self.getTitle(clue)
            if title:
                authors = self.lookupByTitle(title)
                if authors:
                    solutions = self.getPossibleSolutions(authors, wordLength)
                    if "author" in clue.lower() or "writer" in clue.lower():
                        selfConf = 1.0
                    else:
                        selfConf = 0.2
                    return Answers(solutions, selfConf)
            return Answers([], 0.0)
        except:
            return Answers([], 0.0)
        
    def test(self):
        return self.solve('Author of "Gone Girl"', 5)   # Flynn
















