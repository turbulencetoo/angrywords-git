"""
Another solver that uses SphinxSearch to return Wikipedia titles
based on a given clue. This solver is almost identical to BasicSphinx
except it is using a different match mode. It matches looking for a phrase
instead of simply any word in the clue. As a result, the confidences are 
calculated slightly differently

Written by Alex Kuntz
"""


from DataStructure.dataStructure import Answers
import sphinxapi
import re
import itertools

class SphinxPhrase:
	def __init__(self):
		'''
		Connects to Sphinx, using Phrase Match
		'''
		self.__name__ = "sphinxPhrase"
		self.client = sphinxapi.SphinxClient()
		self.client.SetServer('localhost', 9312)
		#Sets matcher to use phrases
		self.client.SetMatchMode(2)
		
	def getPhrases(self, clue):
		'''
		Given a clue, creates "phrases" by looking at capitalized letters.
		Sets the self confidence based on how many of the words were capitalized.
		The more capitalized words, the higher the confidence
		'''
		upperWords = []
		clue = clue.split()
		for word in clue:
			if word[0].isupper():
				upperWords.append(word)
		self.selfConf = len(upperWords)/float(len(clue))
		
		#After finding all the capitalized words, find every permutation of those words
		allPhrases = []
		for L in range(0, len(upperWords)+1):
			for subset in itertools.combinations(upperWords, L):
				finalAnswer = ' '.join(subset)
				if finalAnswer:
					allPhrases.append(finalAnswer)
					
		return allPhrases
		
		
	def getSphinxGuesses(self, phrases):
		'''
		Finds the relevant article titles given the phrases
		'''
		allTitles = []
		for phrase in phrases:
			query = self.client.Query(phrase,'searchtext')
			guesses = query['matches']
			titles = []
			for guess in guesses:
				allTitles.append([guess['attrs']['title'],guess['weight']])
		return allTitles
		
	def formatString(self, string):
		'''
		Removes non letters from strings
		'''
		words = string.lower().split()
		formattedWords = []
		for word in words:
			formattedWords.append(re.sub('[^A-Za-z]+', '', word))
		return formattedWords
			
	def removeDuplicates(self, titles, clue):
		'''
		Removes a guess if it contains a word in the clue
		'''
		clueWords = self.formatString(clue)
		formattedTitles = []
		for title in titles:
			formattedTitle = self.formatString(title[0])
			for word in formattedTitle:
				if word in clueWords:
					formattedTitle.remove(word)
			formattedTitles.append([formattedTitle, title[1]])
		return formattedTitles
		
	def getPermutations(self, wordList, clueLength, value):
		'''
		Given the title of a Wikipedia article, finds every combination of the words
		in the title that are the correct length
		'''
		allPerms = []
		for L in range(0, len(wordList)+1):
			for subset in itertools.combinations(wordList, L):
				finalAnswer = ''.join(subset)
				if len(finalAnswer) == clueLength:
					allPerms.append((finalAnswer, value))
		return allPerms
	
	def formatAnswers(self, formattedTitles, clueLength, clue):
		'''
		Concatinates the confidences of identical answers and ensures that all
		confidences sum to 1
		'''
		allAnswers = []
		#Gets every guess for a given clue, including duplicates
		for title in formattedTitles:
			perms = self.getPermutations(title[0], clueLength, title[1])
			if perms:
				for item in perms:
					allAnswers.append(item)
					
		#Concatinates the confidence of identical guesses			
		answersDict = {}
		seen = set()
		seen_add = seen.add
		allAnswers = [ x for x in allAnswers if x not in seen and not seen_add(x)]
		for answer in allAnswers:
			if answer[0] not in answersDict:
				answersDict[answer[0]] = answer[1]
			else:
				answersDict[answer[0]] += answer[1]
		removedDups = []
		for answer in answersDict:
			removedDups.append((answer, answersDict[answer]))	

		#Ensures taht all confidences sum to 1		
		sum = 0.0
		for answer in removedDups:
			sum += answer[1]
		
		finalAnswer = []
		for answer in removedDups:
			conf = answer[1]/sum
			finalAnswer.append((answer[0], conf))
			
		return finalAnswer
				
		
	def test(self):
		return self.solve('Royal Dutch Airlines letters', 3)
		
	def solve(self, clue, wordLength):
		'''
		Returns the best guesses for a clue and a given number of spaces
		'''
		try:
			phrases = self.getPhrases(clue)
			titles = self.getSphinxGuesses(phrases)
			removedDuplicates = self.removeDuplicates(titles, clue)
			answers = self.formatAnswers(removedDuplicates, wordLength, clue)
			return Answers(answers, self.selfConf)
		except:
			return Answers([], 0.0)
