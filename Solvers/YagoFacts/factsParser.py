"""This code parses the yagoFacts files in order to put it into our database."""

def parse(filename):
	file = open(filename)
	outstring = ''
	for line in file:
		line = line.replace("<", "")
		line = line.replace(">", "")
		line = line.replace("_", " ")
		line = line.split("\t")
		outstring += line[1] + '\t' + line[2] + '\t' + line[3] + '\n'
	outfile = open('/crosswordComps/yagoFacts/parsedFacts.tsv', 'w')
	outfile.write(outstring)
	file.close()
	outfile.close()
	
def main():
	parse("/crosswordComps/yago2s_tsv/newyagoFacts.tsv")

if __name__ == '__main__':
	main()