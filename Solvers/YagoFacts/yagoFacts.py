from DataStructure.dataStructure import Answers
import psycopg2
import re

class YagoFacts:
    def __init__(self):
        self.__name__ = "yagoFacts"
        
    def connect(self):
        """Connect method sets up a connection to the yago database."""
        #Define our connection string
        conn_string = "host='localhost' dbname='yago' user='angrywords' password='dln'"
     
        # get a connection, if a connect cannot be made an exception will be raised here
        self.conn = psycopg2.connect(conn_string)

        
    def select(self, query):
        """Takes in query as parameter, executes query, and returns results."""
        cursor = self.conn.cursor()
    
        # execute our Query
        cursor.execute(query)
     
        # retrieve the records from the database
        return cursor.fetchall()
        
    def parseClue(self, clue):
        """Finds all words beginning with a capital letter, removes 's from words."""
        properNouns = []
        for word in clue.split():
            if word[-2:] == "'s":
                word = word[:-2]
            word = word.replace("'", "''")
            if word[0].istitle():
                properNouns.append(word)
        return properNouns
        
    def solve(self, clue, length):
        """
        Looks up proper nouns in database as subject or fact.
        If found, looks up all other words in relations.  Adjusts confidence
        based on how many proper nouns found and whether any relational words found.
        """
        self.connect()
        parsedClue = self.parseClue(clue)
        results = []
        for c in parsedClue:
            query = "SELECT * FROM yagofacts WHERE lower(subject) LIKE '%{}%';".format(c.lower())
            results.extend(self.select(query))
            query = "SELECT * FROM yagofacts WHERE lower(fact) LIKE '%{}%';".format(c.lower())
            results.extend(self.select(query))
        resultDict = {}
        for r in results:
            # check all other subjects
            splitSubject = re.sub("[,.!?\*:;\'\"()\-]", "", r[0])
            splitSubject = splitSubject.split()
            for word in splitSubject:
                if len(word) == length:
                    score = 1
                    for c in parsedClue:
                        if c in r[0] or c in r[2]:
                            score += 1
                    for w in clue.split():
                        # check if non-nouns in relation
                        if w not in parsedClue and w in r[1]:
                            score += 1
                    if word in resultDict:
                        resultDict[word] += score
                    else:
                        resultDict[word] = score
            # check all other facts         
            splitFact = re.sub("[,.!?\*:;\'\"()\-]", "", r[2])
            splitFact = splitFact.split()
            for word in splitFact:
                if len(word) == length:
                    score = 1
                    for c in parsedClue:
                        if c in r[0] or c in r[2]:
                            score += 1
                    for w in clue.split():
                        # check if non-nouns in relation
                        if w not in parsedClue and w in r[1]:
                            score += 1
                    if word in resultDict:
                        resultDict[word] += score
                    else:
                        resultDict[word] = score
        total = 0.0
        max = 0
        for item in resultDict.keys():
            total += resultDict[item]
            if resultDict[item] > max:
                max = resultDict[item]
        returnList = []
        for item in resultDict.keys():
            returnList.append((item, resultDict[item]/total))
        if returnList:
            return Answers(returnList, max/total)
        else:
            return Answers([], 0)
    
    def test(self):
        """Test function."""
        return self.solve("Tyler, Polk, Pierce", len("PRESIDENT"))