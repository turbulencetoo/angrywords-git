'''
This solver uses SphinxSearch to look for Wikipedia articles that are relevant to
the given clue. Given a clue, it finds relevant Wikipedia articles. We then use the
titles of those articles to form answers for the clue

Written by Alex Kuntz
'''

from DataStructure.dataStructure import Answers
import sphinxapi
import re
import itertools

class BasicSphinx:
    def __init__(self):
        '''
        Connects to Sphinx when created. Uses the standard SphinxSearch algorithm
        '''
        self.__name__ = "basicSphinx"
        self.client = sphinxapi.SphinxClient()
        self.client.SetServer('localhost', 9312)
        
    def getSphinxGuesses(self, clue):
        '''
        Finds the relevant Wikipedia articles from the clue. The articles
        will have a weight based on relevance
        '''
        query = self.client.Query(clue,'searchtext')
        guesses = query['matches']
        titles = []
        for guess in guesses:
            titles.append([guess['attrs']['title'],guess['weight']])
        return titles
        
    def formatString(self, string):
        '''
        Removes any character that is not a letter
        '''
        words = string.lower().split()
        formattedWords = []
        for word in words:
            formattedWords.append(re.sub('[^A-Za-z]+', '', word))
        return formattedWords
            
    def removeDuplicates(self, titles, clue):
        '''
        Removes a guess if it contains a word that was in the clue
        '''
        clueWords = self.formatString(clue)
        formattedTitles = []
        for title in titles:
            formattedTitle = self.formatString(title[0])
            for word in formattedTitle:
                if word in clueWords:
                    formattedTitle.remove(word)
            formattedTitles.append([formattedTitle, title[1]])
        return formattedTitles
        
    def getPermutations(self, wordList, clueLength, value):
        '''
        Given a title of a Wikipedia article, finds all permutations of that title
        that fits in the given number of spaces
        '''
        allPerms = []
        for L in range(0, len(wordList)+1):
            for subset in itertools.combinations(wordList, L):
                finalAnswer = ''.join(subset)
                if len(finalAnswer) == clueLength:
                    allPerms.append((finalAnswer, value))
        return allPerms
    
    def formatAnswers(self, formattedTitles, clueLength, clue):
        '''
        Concatinates the confidences of identical answers and ensures that all
        confidences sum to 1
        '''
        allAnswers = []
        #Gets every guess for a given clue, including duplicates
        for title in formattedTitles:
            perms = self.getPermutations(title[0], clueLength, title[1])
            if perms:
                for item in perms:
                    allAnswers.append(item)
                    
        #Concatinates the confidence of identical guesses           
        answersDict = {}
        seen = set()
        seen_add = seen.add
        allAnswers = [ x for x in allAnswers if x not in seen and not seen_add(x)]
        for answer in allAnswers:
            if answer[0] not in answersDict:
                answersDict[answer[0]] = answer[1]
            else:
                answersDict[answer[0]] += answer[1]
        removedDups = []
        for answer in answersDict:
            removedDups.append((answer, answersDict[answer]))
        
        #Ensures taht all confidences sum to 1
        sum = 0.0
        for answer in removedDups:
        	sum += answer[1]
        
        finalAnswer = []
        for answer in removedDups:
        	conf = answer[1]/float(sum)
        	finalAnswer.append((answer[0], conf))
        	
        return finalAnswer
        	
        
        
    def getSelfConf(self,clue):
        '''
        Calculates the self confidence of the solver. The longer
        the clue, the higher the self confidence.
        '''
        length = len(clue.split())
        if length >= 11:
            selfConf = .1
        else:
            selfConf = 1.0/(11-length)
            if selfConf > .8:
                selfConf = .8
        return selfConf
                
    
    def test(self):
        return self.solve('4 8 15 16 23 42', 5)
        
    def solve(self, clue, wordLength):
        '''
        Returns the best guesses for a clue and a given number of spaces
        '''
    	try:
        	selfConf = self.getSelfConf(clue)
        	titles = self.getSphinxGuesses(clue)
        	removedDuplicates = self.removeDuplicates(titles, clue)
        	answers = self.formatAnswers(removedDuplicates, wordLength, clue)
        	return Answers(answers, selfConf)
        except:
        	return Answers([],0.0)
