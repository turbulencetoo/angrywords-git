#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''
wiktionary.py

Donny Mellstrom
Oct. 2012

Scrapes wiktionary for answers to clues, with a few different approaches.

To Do:

confidences -- beta done
stopword removal
caching and pickling
stemming stuff -- in place but not active
'''

import urllib, urllib2
import json
import pprint
from DataStructure.dataStructure import Answers
import ClueParser.clueParser as cp

import sys, codecs, unicodedata

req_headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13','Referer': 'http://python.org'}

class Wiktionary:

    def __init__(self):
        self.__name__ = "wiktionary"
        self.stemmer = cp.ClueParser()

    def solve(self, clue, length, deep = False):
        try:
            #process the clue?
            processedClue = clue

            #build search query for clue
            encodedQuery = urllib.urlencode([('action','query'),('list','search'),('format', 'json'),('srsearch', processedClue)])
            url = 'http://en.wiktionary.org/w/api.php?' + encodedQuery
            #print url
            #print
            text = self.scrape(url)              #get and parse JSON
            links = self.resultsParser(text)
            #print links, '\n---'
            i = 0
            priorityLinks = [] #will contain the links that directly match clue
            extraLinks = []
            while i < len(links):
                pageText = self.scrape('http://en.wiktionary.org/w/index.php?' + urllib.urlencode([('action', 'raw'),('title', links[i])]))
                pageLinks = self.wikiParser(pageText, length)
                if len(pageLinks) == 0:
                    links.pop(i)            #remove non-English result pages
                else:
                    if links[i].lower() == processedClue.lower():
                        priorityLinks.extend(pageLinks)
                    else:
                        extraLinks.extend(pageLinks)
                    i += 1

            #Gather up answers to return
            results = []
            linkResults = []
            #hi-conf stuff directly from search results
            myConf = 0.5
            if len(priorityLinks) == 0:
                myConf = 0.75
                if len(extraLinks) == 0:
                    myConf = 1.0
            for link in links:
                forms = [link]
                destemmed_list = self.stemmer.destem(result.lower())
                for word in destemmed_list:
                    if word != result and word not in forms:
                        forms.append(word)
                for form in forms:
                    answer = self.flattenToAlpha(form)
                    if len(answer) == length:
                        linkResults.append(answer)
            for i in range(len(linkResults)):
                results.append([linkResults[i], myConf/len(linkResults)])

            #priority links from clue-named pages take precedence
            pConf = 0.4
            if len(extraLinks) == 0:
                pConf = 0.5
            linkResults = []
            for link in priorityLinks:
                forms = [link]
                destemmed_list = self.stemmer.destem(result.lower())
                for word in destemmed_list:
                    if word != result and word not in forms:
                        forms.append(word)
                for form in forms:
                    answer = self.flattenToAlpha(form)
                    if len(answer) == length:
                        linkResults.append(answer)
            for i in range(len(linkResults)):
                results.append([linkResults[i], pConf/len(linkResults)])

            remainingConf = 0.25
            if len(priorityLinks) != 0:
                remainingConf = 0.1

            #lastly, the low-confidence bottom of the barrel
            linkResults = []
            for link in extraLinks:
                forms = [link]
                destemmed_list = self.stemmer.destem(result.lower())
                for word in destemmed_list:
                    if word != result and word not in forms:
                        forms.append(word)
                for form in forms:
                    answer = self.flattenToAlpha(form)
                    if len(answer) == length:
                        linkResults.append(answer)
            for i in range(len(linkResults)):
                results.append([linkResults[i], remainingConf/len(linkResults)])

            '''if len(results) < 1 and not deep:
                #try each individual clue word if no results
                # Low confidence approach!
                words = processedClue.split()

                ## INCORPORATE STOP WORD FCNALITY HERE!!
                for word in words[:3]:
                    wordResults = self.solve(word, length, True)
                    results.extend(wordResults[0])'''

            #need to combine identical answers and their confidences here after conf's have been assigned!
            i = 0
            while i < len(results):
                currRes = results[i][0]
                j = i + 1
                while j < len(results):
                    if results[j][0] == currRes:
                        results[i][1] += results[j][1]
                        results.pop(j)
                    else:
                        j += 1
                i += 1

            #overall conf
            overall = 0.8
            if deep:
                overall = 0.2

            retObj = Answers([(r[0],r[1]) for r in results], overall)
            if len(retObj.answers) == 0 and not deep:
                #try longest clue word if no results
                clueWords = sorted(processedClue.split(), key=lambda x:len(x))
                deepObj = self.solve(clueWords[0], length, True)
                clueWords.pop(0)
                while len(deepObj.answers) == 0 and len(clueWords) > 0:
                    deepObj = self.solve(clueWords[0], length, True)
                    clueWords.pop(0)
                return deepObj
            return retObj

        except Exception as e:
            #print 'Wiktionary solver errored out while solving ', clue, ':'
            #print e
            return Answers([], 0.0)


    def scrape(self, url):
        '''Gets file from the specified URL'''
        try:
            request = urllib2.Request(url, headers=req_headers)
            opener = urllib2.build_opener()
            response = opener.open(request)
            contents = response.read()
            return contents
            
        except Exception as e:
            print "Failed to get url"
            print e
            return None
            
        finally:
            opener.close()

    def resultsParser(self, source, ranked = False):
        '''Searches the JSON for links '''
        parsed = json.loads(source)
        try:
            results = [child[u'title'] for child in parsed[u'query'][u'search']] #Delve into XML tree
            links = {}
            rank = 0
            for page in results:
                #Weed out links with illegal chars
                if (ord(page[0]) < 196 or (ord(page[0]) == 196 and ord(page[1]) < 156)):
                    links[rank] = page
                    rank += 1
                else:
                    pass
                    #print 'Tossing out ', page
            linkList = [links[i] for i in sorted(links.iterkeys())]
            return linkList
        except:
            #print 'Invalid JSON'
            return []

    def wikiParser(self, source, length):
        '''Searches wiki markup for relevant links with len. within 4 of length'''
        engHead = source.find('==English==')            #locate the english section
        hypers = []
        if engHead == -1:
            return hypers
        eomfc = source.find('==')       #find beginning of next section
        while source[eomfc + 2] == '=':
            eomfc = source.find('==', eomfc + 4)
        if eomfc == -1:
            eomfc = len(source) - 1 # if no other lang. is found, then search all the markup
        linkStart = source.find('[[', engHead) + 2
        while(linkStart != -1):
            linkEnd = source.find(']]', linkStart)
            colon = source.find(':', linkStart, linkEnd)
            if colon == -1:
                pipe = source.find('|', linkStart, linkEnd)
                if pipe != -1:
                    linkStart = pipe + 1
                res = source[linkStart:linkEnd].replace('[', '').replace(']', '')
                lenDiff = len(res) - length
                if lenDiff > -5 and lenDiff < 5:
                    hypers.append(res)
            linkStart = source.find('[[', linkEnd + 2)
        return hypers

        

    def flattenToAlpha(self, unicodeInput):
        '''Strips all non-alpha characters from a string, making it suitable as an answer'''
        #remove all accents
        if type(unicodeInput) == str:
            deaccented = unicodedata.normalize('NFKD', unicode(unicodeInput)).encode('ascii', 'ignore')
        else:
            deaccented = unicodedata.normalize('NFKD', unicodeInput).encode('ascii', 'ignore')
        #remove all non alpha and return in xword answer format
        flat = ''.join(e for e in deaccented if e.isalpha())
        return flat.upper()

    def test(self):
        return self.solve('Tall, thin person', 10)

def main():
    reload(sys)
    sys.setdefaultencoding('utf-8')

    sys.stdout = codecs.getwriter('utf8')(sys.stdout)
    sys.stderr = codecs.getwriter('utf8')(sys.stderr)

    pp = pprint.PrettyPrinter(indent=2, depth=3)
    mySolver = Wiktionary()
    answers = mySolver.solve('Tall, thin person', 10)
    pp.pprint(answers.answers)
    answers = mySolver.solve('Consider carefully', 6)
    pp.pprint(answers.answers)
    answers = mySolver.solve('Make after expenses', 3)
    pp.pprint(answers.answers)
    answers = mySolver.solve('Minute arachnid', 4)
    pp.pprint(answers.answers)
    exit()

if __name__ == '__main__':
    main()
