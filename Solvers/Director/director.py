"""
Uses IMDB database to find the name of directors based on the title of a movie or television
show

Written by Alex Kuntz and Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools

class Director:
    def __init__(self):
        self.__name__ = "director"
    
    def directorInClue(self,clue):
        if re.search('director|Director', clue):
            return True
        return False
    
    def directedInClue(self,clue):
        if re.search('directed|Directed|Film|film', clue):
            return True
        return False
    
    def getTitle(self, clue):
        """Given a clue, tries to identify a title in quotes"""
        
        quotes = '"(.+?)"'
        search = re.search(quotes, clue)
        if search:
            return search.group(1)
        return ''
    
    def getDirector(self, clue):
        """Given a clue, identifies capitalized words as possible names"""
        names = []
        for word in clue.split():
            if word[0].istitle():
                names.append(word)
        return names
    
    def getPermutations(self, names):
        """Given a list of possible names, creates all permutations of first and last names"""
        namesToSearch = []
        for item in itertools.permutations(names):
            for i in range(len(item)):
                firstName = item[i:]
                firstName = ' '.join(firstName)
                lastName = item[:i]
                lastName = ' '.join(lastName)
                namesToSearch.append((firstName, lastName))
            namesToSearch.append(('',' '.join(item)))
        return namesToSearch
    
    def lookupTitle(self, directors):
        """
        Given a list of names to search by, tries to find titles that match
        """
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allTitles = []
        namesToSearch = self.getPermutations(directors)
        for name in namesToSearch:
            if name[0] == '':
                query = "SELECT title FROM directors WHERE lname = %s;"
                cursor.execute(query, [name[1]])
            elif name[1] == '':
                query = "SELECT title FROM directors WHERE fname = %s;"
                cursor.execute(query, [name[0]])
            else:
                query = "SELECT title FROM directors WHERE fname = %s and lname = %s;"
                cursor.execute(query, [name[0], name[1]])
            titles = cursor.fetchall()
            for title in titles:
                allTitles.append(title[0])
        conn.close()
        return allTitles 
    
    def lookupDirector(self, title):
        """
        Given a possible movie title, find who directed it
        """
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        query = "SELECT fname, lname FROM directors WHERE title = %s or title = %s;"
        cursor.execute(query, [title, '"'+title+'"'])       # Check for movies or TV shows
        names = cursor.fetchall()
        conn.close()
        return names
        
    def formatTitleAnswers(self, titles, answerLength):
        """
        Given a list of titles returned by the query, find ones that might be the correct answer
        """
        finalTitles = []
        confTitles = []
        for title in titles:
            title = re.sub("\(.+\)", '', title)
            title = re.sub('[^a-zA-Z]', '', title).lower()
            if len(title) == answerLength:
                finalTitles.append(title)
        # Assign uniform confidence
        for title in finalTitles:
            confTitles.append((title, 1.0/len(finalTitles)))
        
        return confTitles
                
        
    def formatDirectorAnswers(self, names, clue, answerLength):
        """
        Given a list of directors returned by the query, find ones that might be the correct answer
        """
        splitClue = clue.lower().split()
        answers = []
        finalAnswers = []
        for name in names:
            # Tries combinations of first name, last name, or both to find the answer
            lastName = re.sub("\(.+\)", '', name[1])
            lastName = re.sub('[^a-zA-Z]', '', lastName).lower()
            if len(lastName) == answerLength and lastName not in splitClue:
                answers.append(lastName)
            firstName = re.sub("\(.+\)", '', name[0])
            firstName = re.sub('[^a-zA-Z]', '', firstName).lower()
            if len(firstName) == answerLength and firstName not in splitClue:
                answers.append(firstName)
            fullName = firstName + lastName
            if len(fullName) == answerLength:
                answers.append(fullName)
        # Assign uniform confidence based on the number of possibilities
        for answer in answers:
            finalAnswers.append((answer, 1.0/len(answers)))
        return finalAnswers
         
    def solve(self, clue, wordLength):
        try:
            finalAnswers = []
            # Handles the case where the answer is probably a director's name
            if self.directorInClue(clue):
                selfConf = 1
                title = self.getTitle(clue)
                names = self.lookupDirector(title)
                answers = self.formatDirectorAnswers(names, clue, wordLength)
                return Answers(answers, selfConf)       
            # Handles the case where the answer is probably a movie title
            elif self.directedInClue(clue):
                selfConf = 1
                directorList = self.getDirector(clue)
                titles = self.lookupTitle(directorList)
                answers = self.formatTitleAnswers(titles, wordLength)
                return Answers(answers, selfConf)
            else:   
                return Answers([], 0.0)
        except:
            return Answers([], 0.0)
            
        
        
    def test(self):
        return self.solve('1997 Steven Spielberg film', 7)
