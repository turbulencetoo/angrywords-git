"""
Uses an IMDB database to lookup all movies given a year

Written by Alex Kuntz and Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re

class MovieByYear:
    def __init__(self):
        self.__name__ = "movieByYear"
        
    def findYear(self, clue):
        '''
        Finds a 4 digit number within the clue
        '''
        exp = '([0-9]{4})'
        year = re.search(exp, clue)
        return year.group(1)
        
    def findMovies(self, clue, year):
        '''
        Finds all movies that were made in the given year
        '''
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allNames = []
        query = "SELECT title FROM movies WHERE movie_year = %s;"
        cursor.execute(query, [year])
        movies = cursor.fetchall()
        conn.close()
        return movies
        
    def solve(self, clue, ansLength):
        try:
            year = self.findYear(clue)
            #If we don't find a year, return empty
            if year:
                finalMovies = []
                movies = self.findMovies(clue, year)
                exp = '[Mm]ovie|[Ff]ilm|[Ss]creenplay'
                #If we find one of the keywords above, we know we are looking for movies
                #so selfConf should be high
                if re.search(exp, clue):
                    selfConf = 1
                else:
                    selfConf = .25
                #Format the titles of movies. Removes all non letters. Checks to see if
                #the movie has a colon in the title and allows appending of everything after
                #the colon
                for movie in movies:
                    movie = movie[0]
                    withColon = re.sub('[^a-zA-Z:]', '', movie)
                    withoutColon = re.sub('[^a-zA-Z]', '', movie)
                    if len(withoutColon) == ansLength:
                        finalMovies.append(withoutColon)
                    elif ":" in withColon:
                        colonIndex = withColon.find(':')
                        if colonIndex == ansLength:
                            finalMovies.append(withColon[:colonIndex])
                
                #Ensures that all confidences sum to 1
                moviesWithConf = []
                total = len(finalMovies)
                for movie in finalMovies:
                    moviesWithConf.append((movie.lower(), 1.0/total))
                return Answers(moviesWithConf, selfConf) 
                
            else:
                return Answers([], 0.0)
        except:
            return Answers([], 0.0)
            
    def test(self):
        return self.solve('1997 Steven Speilberg film', 7)