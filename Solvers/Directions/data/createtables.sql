CREATE TABLE countries
(country varchar(50),
 code char(2),

PRIMARY KEY (code)
);



CREATE TABLE cities
(code char(2),
city varchar(150),
accentcity varchar(150),
region varchar(30),
pop integer,
lat float,
long float,

FOREIGN KEY (code) REFERENCES countries (code)
);

