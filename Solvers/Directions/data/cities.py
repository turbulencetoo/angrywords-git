"""This code is just to pull out cities data for the directions database.
We need cities, latitudes, and longitudes."""

import re
import codecs

text = ''

i = 0

with open('worldcitiespop.txt') as f:
	with codecs.open('cities.data', 'w', encoding='utf-8') as output:
		pattern = re.compile("(?u)[a-z]{2},.+,.+,.+,(?P<pop>[0-9]+),.+,.+")
		for line in f:
			try:
				line = unicode(line)
			except:
				line = line.decode('utf-8', 'replace')
				
			match = re.match(pattern, line)
			if match and int(match.group("pop")) > 20000:
				output.write(line)