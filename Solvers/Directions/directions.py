"""This solver is supposed to return a cardinal direction when given two cities that
a clue might be trying to find a direction between.  It was written early on, and could
have been improved, but by the time we finished high-priority things we realized this
type of solver was less valuable, so we didn't go back."""

import re
import math
import psycopg2
from DataStructure.dataStructure import Answers

class Directions():

    def __init__(self):
        self.__name__ = "directions"

    def findDirection(self, (lat1, long1), (lat2, long2)):
        """Finds direction from point one to point two."""
        #Assumes always going from point1 to point2
        
        direction = ""
        
        dLon = math.radians(long2 - long1)
        
        y = math.sin(dLon) * math.cos(math.radians(lat2));
        x = math.cos(math.radians(lat1)) * math.sin(math.radians(lat2)) - \
            math.sin(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.cos(dLon)
        bearing = math.degrees(math.atan2(y, x))
        norm = (bearing + 360) % 360  
    
        # Second letter
        if lat1 < lat2:
            direction += "N"
        elif lat1 > lat2:
            direction += "S"
        else:
            print "This is probably not a directional clue"
            
        # Third letter
        if bearing < 0:
            direction += "W"
        elif bearing > 0:
            direction += "E"
        else:
            print "This is probably not a directional clue"
        
        # First letter
        if 135 > (norm % 180) > 45:
            direction = direction[1] + direction
        else:
            direction = direction[0] + direction
        
        return direction
        
    def reverseDirection(self, direction):
        """Reverses a direction abbreviation."""
        newDirection = ""
    
        for item in direction:
            if item == "N":
                newDirection += "S"
            elif item == "S":
                newDirection += "N"
            elif item == "E":
                newDirection += "W"
            elif item == "W":
                newDirection += "E"
        
        return newDirection
        
    def solve(self, clue, length):
        """Finds a direction by parsing a clue and finding directions using it."""
        if self.parseClue(clue, length):
            first_place, second_place = self.parseClue(clue, length)
            answers = self.calculateDirection(first_place, second_place)
            return Answers(answers[0], answers[1])
        return Answers([], 0)
        
    def connect(self):
        """Connect method sets up a connection to a latitude and longitude database."""
        #Define our connection string
        conn_string = "host='localhost' dbname='latlong' user='angrywords' password='dln'"
        
        # get a connection, if a connect cannot be made an exception will be raised here
        self.conn = psycopg2.connect(conn_string)
    
            
    def select(self, query):
        """Takes a query as a parameter and creates a cursor in the database to execute that query.
        Returns the query result."""
        cursor = self.conn.cursor()
      
        # execute our Query
        cursor.execute(query)
     
        # retrieve the records from the database
        return cursor.fetchall()
            
    def parseClue(self, clue, length):
        """If two items exist matching the search pattern for places, return them; else return false."""
        if length == 3:
            pattern = re.compile("(?P<first_place>([A-Z][a-z]*)[- ]+)(?:[a-z]*[,.]*[- ])+(?P<second_place>([A-Z][a-z]* *)+)")
            match = pattern.search(clue)
            if match:
                first_place = match.group("first_place").strip().strip("-")
                second_place = match.group("second_place").strip().strip("-")
                return first_place, second_place
            else:
                return False
        return False
    
    def calculateDirection(self, first_place, second_place):
        """Looks up possible place names in databases, first for complete matches, then for partial.
        If any match is found, extracts latitudes and longitudes.
        Then calls findDirection function and reverseDirection function and returns the results."""
        # Look places up in the database
        self.connect()
        query1 = "SELECT city, lat, long FROM cities where city = '%s';" %first_place.lower()
        result1 = self.select(query1)
        if not result1:
            query1 = "SELECT city, lat, long FROM cities;"
            for entry in query1:
                if first_place in entry[0]:
                    result1 = entry
        query2 = "SELECT city, lat, long FROM cities where city = '%s';" %second_place.lower()
        result2 = self.select(query2)
        if not result2:
            query2 = "SELECT city, lat, long FROM cities;"
            for entry in query2:
                if second_place in entry[0]:
                    result2 = entry
        if result1 and result2:
            lat1 = result1[0][1]
            long1 = result1[0][2]
            lat2 = result2[0][1]
            long2 = result2[0][2]
            direction = self.findDirection((lat1, long1), (lat2, long2))
            reverse = self.reverseDirection(direction)
        
            return [[(direction, 2/3.), (reverse, 1/3.)], .8]
        return [[], 0]
        
    def test(self):
        """Tester function."""
        return self.solve("Mad", 3)