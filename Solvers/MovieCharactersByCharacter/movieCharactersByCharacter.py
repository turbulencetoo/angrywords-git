'''
Uses the IMDB database to search for character names given titles and other character names.
For example, given a title of a movie and a last name of a character, we should return the first
name of that character.

By Alex Kuntz and Veronica Lynn
'''

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools

class MovieCharactersByCharacter:
	def __init__(self):
		self.__name__ = "movieCharactersByCharacter"
	
	def getTitle(self, clue):
		'''
		Returns the part of the clue inside quotes (we assume this will be the title)
		'''
		match = re.search('"(.+?)"', clue)
		if match:
			return match.group(1), clue.replace('"' + match.group(1) + '"', '')
		else:
			return '', clue
			
	def getName(self, clue):
		'''
		Returns all capitalized words (we assume this will be actor names)
		'''
		names = []
		for word in clue.split():
			if word[0].istitle() and word != 'Actor' and word != "Actress" and word != "Character":
				if "'" in word:
					index = word.find("'")
					names.append(word[:index])
				else:
					names.append(word)
		
		return names
		
	def lookupCharacterFromActorAndTitle(self, possibleNames, title):
		'''
		Given a title and part of a character name, look up the complete character name
		'''
		conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
		conn = psycopg2.connect(conn_string)
		cursor = conn.cursor()
		allNames = []
		
		for name in possibleNames:
			query = "SELECT char_name FROM actors WHERE char_name ~ %s	and (title = %s or title = %s);"
			cursor.execute(query, [name, title, '"'+title+'"'])
			charNames = cursor.fetchall()
			for charName in charNames:
				if charName[0] not in allNames:
					allNames.append(charName[0])
		conn.close()
		return allNames
	
	def getSubsets(self, things):
		'''
		Compute all subsets of all permutations of a list. We do this so we can return
		first name, last name, or both
		'''
		
		final = []
		for i in range(1,len(things) + 1):
			subsets = list(itertools.combinations(things, i))
			for subset in subsets:
				final += self.getPermutations(subset)
		
		return list(set(final))
		
	def getPermutations(self, names):
		'''
		Returns all permutations of possible names. We do this in case the actor has 
		a middle name or multiple middle names we can still find all combinations of 
		first name and last name.
		'''
		namesToSearch = []
		for item in itertools.permutations(names):
			for i in range(len(item)):
				firstName = item[i:]
				firstName = ' '.join(firstName)
				lastName = item[:i]
				lastName = ' '.join(lastName)
				namesToSearch.append((firstName, lastName))
			namesToSearch.append(('',' '.join(item)))
		return namesToSearch
	
	
	def getNamesFromFullName(self, namesList, clue):
		'''
		Given full names from the database query, return the names that we are interested in.
		For example, if we created the query using the first name, we will want to remove the
		first name from our answer
		'''
		allNames = []
		for name in namesList:
			name = re.sub('[^a-zA-Z]', ' ', name)
			splitNames = name.split()
			for item in splitNames:
				if item.lower() in clue.lower():
					#Removes the name that we used in the search
					splitNames.remove(item)
			
			#Finds all subsets of the remaining names and joins them together
			subsets = self.getSubsets(splitNames)
			for subset in subsets:	  
				nameToAdd = ''.join(subset)
				if nameToAdd not in allNames:
					allNames.append(nameToAdd)
		return allNames
			

	def formatNames(self, names, answerLength):
		'''
		Removes all non letter characters and calculates confidence so they sum to 1
		'''
		allNames = []
		finalNames = []
		for name in names:
			name = re.sub('[^a-zA-Z]', '', name)
			if len(name) == answerLength:
				allNames.append(name)
		for name in allNames:
			finalNames.append((name.lower(),1./len(allNames)))		
		return finalNames
	
	
	def solve(self, clue, ansLength):
		'''
		Returns the best guesses for a clue and a given number of spaces
		'''
		try:
			title, strippedClue = self.getTitle(clue)
			#We need to find a title and a name in the clue, otherwise return empty
			if title:
				namesInClue = self.getName(strippedClue)
				selfConf = 1.0
				#We also need to find a full name after querying the databases
				if namesInClue:
					names = self.lookupCharacterFromActorAndTitle(namesInClue, title) 
					names = self.getNamesFromFullName(names, clue)
					if names:
						answers = self.formatNames(names, ansLength)
						return Answers(answers, selfConf)
					else:
						return Answers([], 0.0)
				else:
					return Answers([], 0.0)
			else:
				return Answers([], 0.0)
		except:
			return Answers([], 0.0)
			
			
	def test(self):
		return self.solve('Thrace of "Battlestar Galactica"', 4)
