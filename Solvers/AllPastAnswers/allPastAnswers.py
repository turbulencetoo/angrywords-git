"""
This solver literally returns all past answers ever seen in a crossword puzzle of the wordlength we're
looking at.	 It's similar to an 'all words' module, but slightly more tailored to crosswords, since it
will have acronyms that crosswords have, and will lack words never seen in crosswords.	The goal is
to fill up spaces that other more accurate solvers are unable to fill--it returns a ton of junk, but it
also is almost always guaranteed to include the correct answer somewhere, which is valuable.
"""

import psycopg2
import sys
import math
from DataStructure.dataStructure import Answers

class AllPastAnswers:

	def __init__(self):
		self.clues = {}
		self.__name__ = "allPastAnswers"
	 
	def connect(self):
		"""Connect method sets up a connection= to pastcrosswords database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='pastcrosswords' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psycopg2.connect(conn_string)

		
	def select(self, query):
		"""Takes in query as parameter, executes query, and returns results."""
		cursor = self.conn.cursor()
	
		# execute our Query
		cursor.execute(query)
	 
		# retrieve the records from the database
		return cursor.fetchall()
		
	def getTopAnswersForLength(self, wordLength):
		"""Returns all answers of correct length and the count of how many
		answers of that length have been seen in puzzles."""
		query = "SELECT * FROM answers WHERE LENGTH(answer) = {};".format(wordLength)
		
		records = self.select(query)
		
		query2 = "SELECT SUM(freq) FROM answers WHERE LENGTH(answer) = {};".format(wordLength)
		
		count = self.select(query2)
		
		return records, count
	
	
	def solve(self, clue, wordLength):
		"""Selects all past answers of the wordlength and assigns them confidences based
		on their frequency as answers."""
		if clue in self.clues:
			return self.clues[clue]
			
		self.connect()
		
		# Get answers that fit the wordLength
		records, count = self.getTopAnswersForLength(wordLength)
		# Sort by frequency
		records.sort(lambda x,y: cmp(x[1], y[1]), reverse=True)
		
		best_guesses = []
		if len(records) > 0:
			count = float(count[0][0])
			
			for answer in records:
				guess = (answer[0], answer[1]/count)
				best_guesses.append(guess)
			
			# since there is so much junk, we make the confidence 0 always!
			confidence = 0.0
			
			self.clues[clue] = Answers(best_guesses, confidence)
			
			self.disconnect()
		else:
			self.clues[clue] = Answers([], 0.0)
		return self.clues[clue]
		
	
	def disconnect(self):
		"""Closes connection."""
		self.conn.close()
				
	def test(self):
		"""Test code, never executed by decider."""
		return self.solve("';", 5)
