from solvertester import *
from Synonym.synonym import *
from PastCrosswords.pastCrosswords import PastCrosswords
from SimilarClues.similarClues import SimilarClues
from Directions.directions import Directions
from AllPastAnswers.allPastAnswers import AllPastAnswers
#from Wikipedia.wikipedia import Wikipedia
from FillInTheBlank.fillInTheBlank import FillInTheBlank
from Director.director import Director
from Actor.actor import Actor
from SportsPlayer.sportsPlayer import SportsPlayer
from Wiktionary.wiktionary import Wiktionary
from Music.music import Music

from Author.author import Author
from BookTitles.bookTitles import BookTitles
from MovieByYear.movieByYear import MovieByYear
from MovieCharactersByActor.movieCharactersByActor import MovieCharactersByActor
from MovieCharactersByCharacter.movieCharactersByCharacter import MovieCharactersByCharacter
from BasicSphinx.basicSphinx import BasicSphinx
from SphinxPhrase.sphinxPhrase import SphinxPhrase
from YagoLinks.yagoLinks import YagoLinks
from YagoFacts.yagoFacts import YagoFacts
#from YagoTypes.yagoTypes import YagoTypes 2slow4me



allSolvers = []
allSolvers.append(Synonym())

allSolvers.append(PastCrosswords())

allSolvers.append(SimilarClues())

allSolvers.append(Directions())

allSolvers.append(AllPastAnswers())

#allSolvers.append(Wikipedia())

allSolvers.append(FillInTheBlank())

allSolvers.append(Director())

allSolvers.append(Actor())

allSolvers.append(SportsPlayer())

allSolvers.append(Wiktionary())

allSolvers.append(Music())

allSolvers.append(MovieByYear())

allSolvers.append(MovieCharactersByActor())

allSolvers.append(MovieCharactersByCharacter())

allSolvers.append(BasicSphinx())

allSolvers.append(SphinxPhrase())

allSolvers.append(YagoLinks())

allSolvers.append(YagoFacts())

#allSolvers.append(YagoTypes())
