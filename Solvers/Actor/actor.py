"""
Uses IMDB database to find the name of actors based on the title of a movie or television
show that they appeared in

Written by Alex Kuntz and Veronica Lynn
"""

from DataStructure.dataStructure import Answers
import psycopg2
import re
import itertools


class Actor:
    
    def __init__(self):
        self.__name__ = "actor"
        
    def getTitle(self, clue):
        '''
        Given a clue, looks for a string that was in quotes. We assume this is the title of the film
        '''
        match = re.search('"(.+?)"', clue)
        if match:
            return match.group(1), clue.replace('"' + match.group(1) + '"', '')
        else:
            return '', clue
    
    
    def getName(self, clue):
        '''
        Given a clue, looks for capitalized words. We assume that this is a first or last name of our actor
        '''
        names = []
        for word in clue.split():
            if word[0].istitle() and word != 'Actor' and word != "Actress":
                names.append(word)
        
        return ' '.join(names)
        
        
    def lookupName(self, name, title):
        '''
        Given a first or last name and a title, retuns the full name of actors who have
        that first or last name and appear in that title
        '''
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        allNames = []
        
        #Assumes the given name is a first name. We have to search for title or "title" because
        #television shows have quotes around them in the database
        query = "SELECT fname FROM actors WHERE lname = %s and (title = %s or title = %s);"
        cursor.execute(query, [name, title, '"'+title+'"'])
        
        fnames = cursor.fetchall()
        for fname in fnames:
            allNames.append(fname[0])
        
        #Same query except we assume the given name is a last name
        query = "SELECT lname FROM actors WHERE fname = %s and (title = %s or title = %s);"
        cursor.execute(query, [name, title, '"'+title+'"'])

        lnames = cursor.fetchall()
        for lname in lnames:
            allNames.append(lname[0])
        conn.close()
        return allNames 
    
    def formatNames(self, names, answerLength):
        '''
        Removes all non letters from the name
        '''
        allNames = []
        finalNames = []
        for name in names:
            name = re.sub('[^a-zA-Z]', '', name)
            if len(name) == answerLength:
                allNames.append(name)
        for name in allNames:
            finalNames.append((name.lower(),1./len(allNames)))      
        return finalNames
    
    def formatNamesWithNumMovies(self, names, answerLength):
        '''
        Updates the confidence of each name based on how many movies they have appeared in.
        Ensures that all of the confidences still sum to 1.
        '''
        allNames = []
        finalNames = []
        totalMovies = 0.0
        for name, numMovies in names:
            name = re.sub('[^a-zA-Z]', '', name)
            if len(name) == answerLength:
                allNames.append((name, numMovies))
                totalMovies += numMovies
        for name, numMovies in allNames:
            finalNames.append((name.lower(),numMovies/totalMovies))     
        return finalNames
        
        
    def getActors(self, title):
        '''
        Given a title, finds all actors who appeared in their title. Returns a list of tuples
        of those actors and how many movies they have appeared in
        '''
        conn_string = "host='localhost' dbname='movies' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        #Finds all actors who have appeared in the given title
        allNames = []
        query = "SELECT fname, lname FROM actors WHERE title = %s or title = %s;"
        cursor.execute(query, [title, '"'+title+'"'])
        
        
        names = cursor.fetchall()
        #Finds how many movies those actors have appeared in
        for name in names:
            query = "SELECT count(*) from actors where fname = %s and lname = %s;"
            cursor.execute(query, [name[0],name[1]])
            numMovies = cursor.fetchall()
            allNames.append((name[0], float(numMovies[0][0])))
            allNames.append((name[1], float(numMovies[0][0])))
            allNames.append((' '.join(name), float(numMovies[0][0])))
            
        return allNames
            
    def solve(self, clue, wordLength):
        try:
            #We must have a title, otherwise return nothing
            title, strippedClue = self.getTitle(clue)
            if title:
                name = self.getName(strippedClue)
                selfConf = 1.0
                if name:
                    #If we find a name, check to see if we can get a first and last name from the title we found earlier
                    names = self.lookupName(name, title)
                    if names:
                        #If we find a first and last name, look at all permutations to find the correct length
                        answers = self.formatNames(names, wordLength)
                        return Answers(answers, selfConf)
                    else:
                        #If we don't find a first and last name, look at all actors in the given title
                        names = self.getActors(title)
                        answers = self.formatNamesWithNumMovies(names, wordLength)
                        return Answers(answers, selfConf)
                else:
                    #If we dont' find a first and last name, look at all actors in the given title
                    names = self.getActors(title)
                    answers = self.formatNamesWithNumMovies(names, wordLength)
                    return Answers(answers, selfConf)
            else:
                return Answers([], 0.0)
        except:
            return Answers([], 0.0)
    def test(self):
        return self.solve('"Lost" actor O\'Quinn', 5)
        