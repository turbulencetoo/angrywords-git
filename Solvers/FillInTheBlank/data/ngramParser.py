import re
import sys

def main():
    file = open(sys.argv[1], 'r')
    outfile = open("parsed"+sys.argv[1], 'a')
    
    for line in file: 
        line = re.sub("2008", r"2008\t", line)
        if re.compile("1[89][0-9][0-9]") in line or re.compile("20[0-9][^8]") in line:
            continue
        itemList = line.split("\t")
        itemList[0] = re.sub("^(\W*)", r"\1 ", itemList[0])
        itemList[0] = re.sub("(\W*)$", r" \1", itemList[0])
        wordList = itemList[0].split(" ")
        wordString = ""
        for word in wordList:
            wordString += word + "\t"
#        print itemList[0] + "\t" + wordString + itemList[2]+"\n"
        outfile.write(itemList[0] + "\t" + wordString + itemList[2]+"\n") 
        
    file.close()
    outfile.close()
    
if __name__ == "__main__":
    main()