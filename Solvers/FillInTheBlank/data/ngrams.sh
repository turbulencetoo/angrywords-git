#!/bin/csh

COUNT=0
END=799
while [ $COUNT -le $END ]
do
echo "-------------------------- count = $COUNT -----------------------"
curl -O http://commondatastorage.googleapis.com/books/ngrams/books/googlebooks-eng-us-all-5gram-20090715-$COUNT.csv.zip
unzip googlebooks-eng-us-all-5gram-20090715-$COUNT.csv.zip
awk -Ft '$2 == 2008' googlebooks-eng-us-all-5gram-20090715-$COUNT.csv > fivegram$COUNT.csv
rm  googlebooks-eng-us-all-5gram-20090715-$COUNT.csv.zip
rm  googlebooks-eng-us-all-5gram-20090715-$COUNT.csv
COUNT=$(($COUNT+1))
done
