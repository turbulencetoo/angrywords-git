CREATE TABLE onegrams
(gramid integer,
ngram varchar(200),
freq integer,

PRIMARY KEY (gramid)
);

CREATE TABLE onewords
(gramid integer,
word varchar(200)
);

CREATE TABLE twograms
(gramid integer,
ngram varchar(300),
freq integer,

PRIMARY KEY (gramid)
);

CREATE TABLE twowords
(gramid integer,
word varchar(200)
);

CREATE TABLE threegrams
(gramid integer,
ngram varchar(400),
freq integer,

PRIMARY KEY (gramid)
);

CREATE TABLE threewords
(gramid integer,
word varchar(200)
);

CREATE TABLE fourgrams
(gramid integer,
ngram varchar(500),
freq integer,

PRIMARY KEY (gramid)
);

CREATE TABLE fourwords
(gramid integer,
word varchar(200)
);

CREATE TABLE fivegrams
(gramid integer,
ngram varchar(600),
freq integer,

PRIMARY KEY (gramid)
);

CREATE TABLE fivewords
(gramid integer,
word varchar(200)
);

CREATE TABLE word_frequency
(wordid integer,
word varchar(200),
frequency integer,

PRIMARY KEY (wordid)
);
