import re
import os

def main():
	words = open("word_freq.txt", "a")
	freq_dict = {}
	for f in range(5):
		print f
		unique_value = 0
		ngram_file = open("parsed%dgrams.txt"%(f+1), "a")
		word_file = open("%dwords.txt"%(f+1), "a")
		for filename in os.listdir("%dgrams"%(f+1)):
			original_file = open("%dgrams/"%(f+1)+filename, "r")
			for line in original_file:
				if re.search("[^\w\s]", line):
					continue
				line = line.strip()
				line = re.sub("[?!.,]", "", line)
				line = line.split("\t")
				ngram_line = ""
				ngram_line += str(unique_value) + "\t"
				gram = ""
				for word in line[0].split(" "):
					gram += word + " "
				#gram = gram[:-1]
				ngram_line += gram + "\t" + line[2] + "\n"
	
				word_line = ""
				for word in line[0].split(" "):
					word_line += str(unique_value)  + "\t" + word + "\n"
				word_file.write(word_line)
		
				ngram_file.write(ngram_line)
				unique_value += 1
				for word in line[0].split(" "):
					if word in freq_dict:
						freq_dict[word] += 1
					else:
						freq_dict[word] = 1
	ngram_file.close()
	keys = freq_dict.keys()
	unique_value = 0
	for key in keys:
		freq_string = str(unique_value) + "\t"
		unique_value += 1
		value = freq_dict.get(key)
		freq_string += key + "\t" + str(value) + "\n"
		words.write(freq_string)
	words.close()
		
		
if __name__ == main():
	main()
		