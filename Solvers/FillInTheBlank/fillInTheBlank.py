# Module uses Google ngram data from 2008 to solve fill-in-the-blank questions.
# If there isn't a blank in the clue, I run the solver again on the clue with
# a blank added to the beginning and a blank added at the end. I still don't 
# think doing so has ever returned a correct answer.

import unicodedata
import re
import psycopg2 as psy
from DataStructure.dataStructure import Answers
from nltk.corpus import stopwords as sw

class FillInTheBlank:
	def __init__(self):
		self.__name__ = "fillInTheBlank"
		
	def connect(self):
		"""Connect method sets up a connection to an ngram database."""
		#Define our connection string
		conn_string = "host='localhost' dbname='ngrams' user='angrywords' password='dln'"
	 
		# get a connection, if a connect cannot be made an exception will be raised here
		self.conn = psy.connect(conn_string)
		
	def shorten_clue(self, clue):
		"""Removes some punctuation from clues and parses multiple words."""
		split_clue = [l for l in clue]
		space_count = 0
		hint = False
		letter = 0
		while letter < len(split_clue):
			if split_clue[letter] == "(":
				hint = True
				split_clue.pop(letter)
				continue
			if split_clue[letter] == ")":
				hint = False
				split_clue.pop(letter)
				continue
			if hint == True:
				split_clue.pop(letter)
				continue
			if split_clue[letter] == " ":
				space_count += 1
			letter += 1
		clue = ''
		clue = ''.join(l for l in split_clue)
		if space_count > 4:
			split_clue = clue.split(' ')
			for word in range(len(split_clue)):
				if "_" in split_clue[word]:
					if word > 0 and word < len(split_clue)-1:
						clue = "%s %s %s"%(split_clue[word-1], split_clue[word], split_clue[word+1])
					elif word == 0:
						clue = "%s %s %s %s"%(split_clue[word], split_clue[word+1], split_clue[word+2], split_clue[word+3])
					else:
						clue = "%s %s %s %s"%(split_clue[word-3], split_clue[word-2], split_clue[word-1], split_clue[word])
		clue = re.sub("	 +", " ", clue)
		clue = clue.strip()
		return clue
		
	def query_clue(self, clue):
		"""Find any length fill in the blank in ngram database.
		Return list of all possible ngrams"""
		self.connect()
		cursor = self.conn.cursor()
		
		clue = clue.replace("'", "''")
		split_clue_one = clue.split("_")
		split_clue_two = []
		for c in range(len(split_clue_one)):
			if split_clue_one[c]:
				split_clue_two.extend(split_clue_one[c].strip().split(" "))
		result_list = []
		# Building query
		for table_name in ['two','three', 'four', 'five']:
			query_part_one = 'FROM '
			query_part_two = 'WHERE '
			for i in range(len(split_clue_two)):
				# Checks if each word is in an ngram
				query_part_one += "%swords AS %s "%(table_name, chr(i+97))
				query_part_two += "lower(%s.word) = '%s' "%(chr(i+97), split_clue_two[i])
				if i > 0:
					query_part_one += "ON %s.gramid = %s.gramid "%(chr(i+96), chr(i+97))
				if i < len(split_clue_two)-1:
					# Checks if the words are in the same ngram
					query_part_one += "JOIN "
					query_part_two += "and "
			query = "SELECT a.gramid " + query_part_one + query_part_two + ';' 
			#print query
			cursor.execute(query)
			query_result = cursor.fetchall()
			id_list = []
			for i in range(len(query_result)):
				id_list.append(query_result[i][0])
			for id in id_list:
				# For every ngram with all the words, get the ngram
				query = "SELECT ngram, freq FROM %sgrams WHERE gramid = %s;"%(table_name, id)
				cursor.execute(query)
				query_result = cursor.fetchall()
				for result in query_result:
					result_list.append(result)
			
		self.conn.close()
		return result_list
		
	def query_words(self, word):
		"""Gets frequency of single words for clues without fill in the blanks."""
		self.connect()
		word = word.replace("'", "''")
		split_word = word.split()
		results = []
		for part in split_word:
			query = "SELECT frequency FROM word_frequency WHERE lower('%s') = lower(word)"%part
			cursor = self.conn.cursor()
			cursor.execute(query)
			x = cursor.fetchall()
			results.append(max(x)[0])
		return_value = 0
		for result in results:
			return_value += result
		self.conn.close()
		return return_value/len(results)
		
	def solve(self, clue, numSpaces):
		"""Checks if _ in clue and either tries to solve clue with blank in that location or at the beginning or end."""
		#assume clue is of the form "I dont know what _" or "Please don't _ me" or "Take my _, please"
		#get rid of all punctuation in the file ()"';:<>,.?/! before we search it with a regex
		clue = re.sub("[,.!?\"*]", "", clue)
		clue = re.sub("_+", "_", clue)
		clue = clue.lower()
		
		# Add a blank if there isn't one already
		if "_" not in clue:
			blankAtStart = "_ " + clue
			blankAtEnd = clue + " _"
			results = map(self.solve, [blankAtStart, blankAtEnd], [numSpaces]*2)
			answerOne = results[0]
			answerTwo = results[1]
			for tuple in answerTwo.answers:
				answerOne.answers.append(tuple)
			total_frequency = 0
			# Confidences are a little weird in this case.
			if answerOne.answers:
				for i in range(len(answerOne.answers)):
					frequency = self.query_words(answerOne.answers[i][0])
					total_frequency += frequency
					answerOne.answers[i] = (answerOne.answers[i][0], frequency)
				for i in range(len(answerOne.answers)):
					answerOne.answers[i] = (answerOne.answers[i][0], answerOne.answers[i][1]/float(total_frequency))
				answerOne.confidence = (1-(1/float(total_frequency**.01)))
				return answerOne
			else:
				return Answers([], 0)
		
		# Remove hints and shorten if more than 5 words
		clue = self.shorten_clue(clue)
		
		# Perform database query
		query_result = self.query_clue(clue)
		
		# Which query results match the original clue (there might be a lot of junk) 
		regex = "\\b" + re.sub("_+", "(" + "[a-zA-Z] *" * (numSpaces-1) + "[a-zA-Z]\\\\b)", clue) + "\\b"
		regex = re.compile(regex)
		possible_answers = {}
		
		for i in range(len(query_result)):
			match = None
			temp_result = re.sub("[^A-Za-z ]", "", query_result[i][0])
			match = regex.findall(temp_result.lower())
			if match:
				match = match[0]
				if match in possible_answers:
					possible_answers[match] += query_result[i][1]
				else:
					possible_answers[match] = query_result[i][1]
					
		# Put together the confidences			
		answers = []
		total_frequency = 0
		for answer in possible_answers.keys():
				answers.append((answer, possible_answers[answer]))
				total_frequency += possible_answers[answer]

		for i in range(len(answers)): 
			answers[i] = (answers[i][0],answers[i][1]/float(total_frequency))
			
		#DONE!	
		if answers:
			return Answers(answers, (1-(1/float(total_frequency**.05))))
		else:
			return Answers([], 0)


	def test(self):
		finalAnswer = self.solve("the sound of _", 5)
		return finalAnswer
