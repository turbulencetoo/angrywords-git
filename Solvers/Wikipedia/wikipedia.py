import psycopg2
from DataStructure.dataStructure import Answers
from ClueParser.clueParser import ClueParser
import re
import time

class Wikipedia:
    def __init__(self):
        self.cleanLinkRE = '(\[\[([A-Za-z]+?)\|([^\]]+?)\]\])'
        self.soloLink = '(\[\[([A-Za-z]+?)\]\])'
        self.wikiArticles = []
        self.stopwords = [word.strip().lower() for word in open('Solvers/stopwords.txt')]
        self.__name__ = "wikipedia"
        
        
    def openPostgres(self):
        """Sets up and returns a connection."""
        conn_string = "host='localhost' dbname='wikipedia' user='angrywords' password='dln'"
        conn = psycopg2.connect(conn_string)
        return conn
        
    def findTitles(self,clue):
        '''
        Finds all Wikipedia articles that contains at least one similar noun from the given clue
        '''
        
        allArticles = []      
        conn = self.openPostgres()
        cursor = conn.cursor()
        for word in clue.split():
            if word.lower() not in self.stopwords:
                 word = word.title()
                 query = "SELECT body, title FROM searchtext WHERE title = '{}';".format(word)
                 cursor.execute(query)
                 articles = cursor.fetchall()
                 for article in articles:
                    allArticles.append(article)
        conn.close()
        return allArticles
        
    def findLinks(self, bodies, length):
        conn = self.openPostgres()
        cursor = conn.cursor()
        allLinks = {}
        titlePair = {}
        total = 0.0
        selfConf = 0.0
        for body,title in bodies:
            
            finalLinks = []
            doubleLinks = re.findall(self.cleanLinkRE, body)
            for link in doubleLinks:
                finalLinks.append(link[-2])
            singleLinks = re.findall(self.soloLink, body)
            for link in singleLinks:
                finalLinks.append(link[-1])
            
            for parsedLink in finalLinks:
                spacedLink = parsedLink
                parsedLink = parsedLink.replace(' ','')
                if "|" not in parsedLink and '\\' not in parsedLink and parsedLink != '' and len(parsedLink) == length:
                    
                    if parsedLink not in allLinks:
                        allLinks[parsedLink] = 1.0
                    else:
                        allLinks[parsedLink] += 1.0
                        
                    total += 1
                    
                    if (spacedLink, title) not in titlePair:
                        titlePair[(spacedLink, title)] = self.checkArticleForLink(title,spacedLink, cursor)
                        if titlePair[(spacedLink, title)]:
                            selfConf += 1
                    
                    if titlePair[(spacedLink, title)]:
                        total += 1.5
                        allLinks[parsedLink] +=1.5
                        
        conn.close()
        
        if total == 0:
            return [], 0.0
        
        tupleList = []
        for guess in allLinks:
            tupleList.append((guess, allLinks[guess]/total))
        tupleList.sort(key=lambda x: x[1])
        tupleList.reverse()
        selfConf = selfConf/len(tupleList)
        if selfConf == 0:
            selfConf = .1
        return tupleList, selfConf
        
    def checkArticleForLink(self, searchTerm, article, cursor):
        queryTime = time.time()
        article = article.title()
        query = "SELECT body FROM searchtext WHERE title = '{}' limit(1);".format(article)
        cursor.execute(query)
        fetch = cursor.fetchall()
        parseTime = time.time()
        if fetch != []:
            body = fetch[0][0]
            
            finalLinks = []
            
            doubleLinks = re.findall(self.cleanLinkRE, body)
            for link in doubleLinks:
                finalLinks.append(link[-2])
            singleLinks = re.findall(self.soloLink, body)
            for link in singleLinks:
                finalLinks.append(link[-1])
            for parsedLink in finalLinks:
                if parsedLink.lower() == searchTerm.lower():
                    return True
        return False
                
    def test(self):
        start = time.time()
        titles = self.findTitles('Darts venue')
        tupleList, selfConf = self.findLinks(titles, 3)
        print time.time()-start
        return Answers(tupleList, selfConf) 
    
    def solve(self, clue, numSpaces):
        try:
            titles = self.findTitles(clue)
            tupleList, selfConf = self.findLinks(titles, numSpaces)
            return Answers(tupleList, selfConf)
        except:
            print "ERROR:", clue
            return Answers([], 0.0)
