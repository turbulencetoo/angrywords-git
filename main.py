#mainMain.py
#calls the main() function in the program specified by sys.argv[1]

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--modName", type=str)
args, unknown = parser.parse_known_args()

modName = args.modName  
#turn the file path into a python path
modName = re.sub("\.py", "", modName)
modName = re.sub("/", ".", modName)

mod = __import__(modName, globals(), locals(), [1], -1)
mod.main()
