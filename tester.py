import sys
from Solvers import *

from DataStructure.dataStructure import Answers

def main():
    solver = eval(sys.argv[1].strip() + '()') # don't do this, kids
    answers = solver.test()
    print answers.getAnswers()
    print answers.getConfidence()
    
if __name__ == "__main__":
    main()