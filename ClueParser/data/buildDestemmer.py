"""
This code was used to generate a destemmer.
The destemmer is a function in the clue parser that is called by a number of solvers.
Essentially, it takes a word/stem and returns all the possible variations on the word.
"""

import nltk
from collections import *

# use porter stemmer to stem words
from nltk.stem.porter import PorterStemmer

def stem(stemmer, text):
	"""Stems the text (obviously)"""
	stemdict = defaultdict(list)
	for token in text:
		token = token.lower()
		if token.isalpha():
			if token not in stemdict[stemmer.stem(token)]:
				stemdict[stemmer.stem(token)] .append(token)
	return stemdict

words = []
# adding lots of word corpuses--all the ones we could find in nltk
for item in nltk.corpus.gutenberg.words():
	words.append(item)
for item in nltk.corpus.brown.words():
	words.append(item)
for item in nltk.corpus.inaugural.words():
	words.append(item)
for item in nltk.corpus.reuters.words():
	words.append(item)
for item in nltk.corpus.switchboard.words():
	words.append(item)
for item in nltk.corpus.words.words():
	words.append(item)
for item in nltk.corpus.movie_reviews.words():
	words.append(item)
stemmer = PorterStemmer()
# stem every word in the corpuses
dict = stem(stemmer, words)
file = open('stemdata', 'a')
for item in dict.keys():
	string = item + '\t'
	for i in dict[item]:
		string += i + ' '
	file.write(string+'\n')
file.close()