"""
The clue parser is a collection of functions that can be used to gather information about a clue
or answer.	The information is all natural language in nature--part of speech, stems, destems, etc.
"""

import psycopg2 as psy
from nltk.stem.porter import PorterStemmer
from nltk import word_tokenize, pos_tag, FreqDist

class ClueParser:
	
	def __init__(self):
		"""Make clueparser"""
		
		self.stemmer = PorterStemmer()
		
	def tokenize(self, text):
		"""Given text, returns the list of tokens""" 
	
		return word_tokenize(text)
		
	def pos_tag(self, text):
		"""Returns a list of tuples with (token, part of speech)"""
			 
		tokens = self.tokenize(text)
		return pos_tag(tokens)
		
	def stem(self, text):
		"""Stems the text (obviously)"""
		
		tokens = self.tokenize(text)
		stemmed_text = ''
		for token in tokens:
			if token.isalpha():
				stemmed_text += self.stemmer.stem(token)
				stemmed_text += ' '
			else:
				stemmed_text += token
				stemmed_text += ' '
		return stemmed_text
		
	def cluePartOfSpeech(self, clue):
		"""Given a clue, returns its part of speech distribution as percentages"""
	
		clue_tagged = self.pos_tag(clue)
		tags = [tag for (token, tag) in clue_tagged if tag.isalpha()]
		total = float(len(tags))
		dist = FreqDist(tags)
		return [(a, b/total) for (a, b) in dist.items()]
		
	def answerMatchesClue(self, clue_percentages, answer_tag):
		"""Helper function for partOfSpeech and verbTense"""
		
		answer_prob = 0.0
		for (tag, prob) in clue_percentages:
			if tag == answer_tag:
				answer_prob = prob
		return answer_prob
		
	def partOfSpeech(self, clue, answer):
		"""Returns the likelihood that the answer is the correct part of speech"""
	
		clue_percentages = self.cluePartOfSpeech(clue)
		answer_tag = self.pos_tag(answer)[0][1]
		return self.answerMatchesClue(clue_percentages, answer_tag)
	   
	
	def clueVerbTense(self, clue):
		"""Given a clue, returns its verb tense distribution as percentages"""
	
		clue_tagged = self.pos_tag(clue)
		verbs = [b for (a, b) in clue_tagged if b[:2] == "VB"]
		total = float(len(verbs))
		dist = FreqDist(verbs)
		return [(a, b/total) for (a, b) in dist.items()]
	
	def destem(self, answer):
		"""Given a word, stems that word and returns all destemmed versions in the database"""
		conn = psy.connect("host='localhost' dbname='destemmer' user='angrywords' password='dln'")
		cursor = conn.cursor()
		stemmed_answer = self.stem(answer).strip()
		query = "SELECT destemmed_words FROM stems WHERE stem = '%s';"%stemmed_answer
		cursor.execute(query)
		words = cursor.fetchall()
		if words:
			words = words[0][0].split()
			return words
		return [answer]
		
	def verbTense(self, clue, answer):
		"""Returns the likelihood that the answer is the correct verb tense"""
		
		clue_percentages = self.clueVerbTense(clue)
		answer_tag = self.pos_tag(answer)[0][1]
		return self.answerMatchesClue(clue_percentages, answer_tag)
	
if __name__ == "__main__":
	cp = ClueParser()
	print cp.partOfSpeech("Veronica Lynn, for example", "awesome")
	print cp.verbTense("Jogging, for example", "running")
	print cp.destem("graceful")