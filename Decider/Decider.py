#decider.py

from Solvers import *
from DataStructure.dataStructure import *
from Resolver.resolver import Resolver
import pickle

class Decider(object):
    def __init__(self, puzzle, readFromFlatFile = True):
        '''
        Param: puzzle
        Should be instance of the AWPuzzle class
        '''
        self.puzzle = puzzle
        self.solvers = allSolvers #imported from Solvers/__init__.py
        self.clueNumAnswersDict = {} # str -> list[tuple(str, float)] {"44a" : [("clue", .020), ...], ...}
        self.answerToSolversDict = {} # tuple[str] -> list[string]
        #maps (clue, ans) to list of solver names that solved clue with ans.
        
        self.querySolvers(readFromFlatFile) 
        
        
    def querySolvers(self, readFromFlatFile):
        '''
        Builds self.clueNumAnswersDict by querying each solver in Solvers/__init__.py
        Also builds the answerToSolversDict by keeping track of which solvers gave which answers
          to a given clue.
        '''
        fileName = "FlatFiles/" + self.puzzle.name + ".answers"
        solverDictFile = "FlatFiles/" + self.puzzle.name + ".solverDict"
        if readFromFlatFile:
            print "Loading answers from flat files..."
            with open(fileName) as f:
                self.clueNumAnswersDict = pickle.load(f)
            print "..."
            with open(solverDictFile) as f:
                self.answerToSolversDict = pickle.load(f)
            print "."
            return
        
        clueDict = self.puzzle.clueDict
        
        for clueKey in clueDict.keys():
            
            clueObj = clueDict[clueKey]
            length = len(clueObj)
            clue = clueObj.clue
            
            try:
                print "Trying clue:", clue
            except:
                print 'Unicode Problem Encountered'
                
            answerDict = {}
            goodSolvers = set(["allPastAnswers", "similarClues", "pastCrosswords"])
            
            for solver in self.solvers:
                print "\tTrying Solver:", solver.__name__
                try:
                    rawAnswer = solver.solve(clue, length)
                except Exception:
                    rawAnswer = Answers([], 0.0)
                    
                solverConfidence = rawAnswer[1]
                
                i = 0
                for tuple in rawAnswer[0]:
                    if i > 15 and solver.__name__ not in goodSolvers:
                        break
                    
                    ans = tuple[0].upper()
                    num = tuple[1] * solverConfidence 

                    if self.answerInClue(ans, clueKey):
                        continue
                    
                    if solver.__name__ == "pastCrosswords" or solver.__name__ == "similarClues":
                        num = num * 3
                    if ans in answerDict:
                        answerDict[ans] += num
                    else:
                        answerDict[ans] = num
                    
                    if (clueKey, ans) in self.answerToSolversDict:
                        self.answerToSolversDict[(clueKey, ans)].append(solver.__name__)
                    else:
                        self.answerToSolversDict[(clueKey, ans)] = [solver.__name__]
                    i += 1
                    
            answerList = sorted(answerDict.items(), key=lambda tuple: tuple[1], reverse=True)
            self.clueNumAnswersDict[clueKey] = answerList
            
        with open(fileName, "w") as f:
            pickle.dump(self.clueNumAnswersDict, f)
        with open(solverDictFile, "w") as f:
            pickle.dump(self.answerToSolversDict, f)
            
        print "sucessfully dumped to flat file: " + fileName
               
    def solvePuzzle(self, readFromFlatFile):
        '''
        Returns the solution by solving all the corners and merging the solutions
        '''            
        cornerSolutions = self.solveAllCorners(readFromFlatFile)
        solution = self.mergeAll(cornerSolutions, True)
        return solution
    
    def solveAllCorners(self, readFromFlatFile):
        '''
        Invokes the corner finder, then invokes solveCorner on each corner
        Solutions to the corners are dumped to a flat file
        '''
        
        fileName = "CornerSolutions/" + self.puzzle.name + ".corner"
        if readFromFlatFile:
            print "Loading solutions to corners..."
            with open(fileName, 'r') as tempfile:
                cornerSolutions =  pickle.load(tempfile)
            print "."
        else:
            corners = self.findCorners()
            
            cornerSolutions = [] # A list of lists, where sublists are possible solutions for a given corner.
            for corner in corners:
                print "I'm trying a corner with", corner
                solved = self.solveCorner(corner)
                if solved:
                    cornerSolutions.append(solved)
                    print "found a solution for corner containing:", corner
                else:
                    pass
#                    print "no solution found for:", corner
            with open(fileName, "w") as tempfile:
                pickle.dump(cornerSolutions, tempfile)
            
            for puzList in cornerSolutions:
                for puz in puzList:
                    puz.updateCoordsToConfDict()
            with open(fileName, "w") as tempfile:
                pickle.dump(cornerSolutions, tempfile)
            print "successfully dumped to", fileName
            
        return cornerSolutions
                
    def solveCorner(self, corner):
        '''
        Solves a corner by invoking a recursive call on the best answer for each clue in a corner.
        If the corner only has one clue in it, it's because the answer to that clue is long
        '''
        possibleSolutions = set([])
        visitedPuzzles = set([])
        if len(corner) == 1:
            clueKey = list(corner)[0]
            for answer, confidence in self.clueNumAnswersDict[clueKey]:
                curPuzzle = self.puzzle.clone()
                curPuzzle.fillClue(clueKey, answer)
                print curPuzzle
                curPuzzle.confidence += confidence
                curPuzzle.confDict[clueKey] = confidence
                possibleSolutions.add(curPuzzle)
                return sorted(possibleSolutions, key=lambda puzz : puzz.confidence, reverse=True)
            
        else:
            for clueKey in corner:
                guess = self.clueNumAnswersDict[clueKey][0][0]
                conf = self.clueNumAnswersDict[clueKey][0][1]
                self.solveCornerRecursive(clueKey, guess, conf, self.puzzle, corner,
                                               self.clueNumAnswersDict, visitedPuzzles, possibleSolutions)
        return sorted(possibleSolutions, key=lambda puzz : puzz.confidence, reverse=True)
            
    def solveCornerRecursive(self, clueKey, answer, confidence, prevPuzzle, corner,
                                  clueKeyAllowedAnswersDict, visitedPuzzles, possibleSolutions):
        '''
        Recursively adds clues to the puzzle until the corner is filled or no further progress can be made.
        '''
        curPuzzle = prevPuzzle.clone()
        curPuzzle.fillClue(clueKey, answer)
        print curPuzzle
        curPuzzle.confidence += confidence
        curPuzzle.confDict[clueKey] = confidence
        
        if curPuzzle in visitedPuzzles:
            print "already been here... backtracking"
            return False
        visitedPuzzles.add(curPuzzle)
        if len(corner - curPuzzle.solvedClues) == 0:
            possibleSolutions.add(curPuzzle)
            print "possible solution found"
            return True
        
        cluesThatIntersect = set([])
        for solvedClueKey in curPuzzle.solvedClues:
            for crossingClueKey in self.puzzle.crossesDict[solvedClueKey]:
                if crossingClueKey not in curPuzzle.solvedClues and crossingClueKey in corner:
                    cluesThatIntersect.add(crossingClueKey)
                    
        clueKeyStillAllowedAnswersDict = {} # str-> list[tuple(str, float)]  
        #this dict maps clue keys to the (clue, conf) pairs that fit            
        
        for crossingClueKey in cluesThatIntersect: 
            allowedAnswers = [] # list[tuple(str, float)]
            for answer, confidence in clueKeyAllowedAnswersDict[crossingClueKey]:
                if curPuzzle.answerFits(crossingClueKey, answer):
                    allowedAnswers.append((answer, confidence))
            if len(allowedAnswers) == 0:
                return False
            allowedAnswers = sorted(allowedAnswers, key=lambda tuple: tuple[1], reverse=True)
            clueKeyStillAllowedAnswersDict[crossingClueKey] = allowedAnswers
            
        for nonCrossingClueKey in corner - cluesThatIntersect - curPuzzle.solvedClues:
            clueKeyStillAllowedAnswersDict[nonCrossingClueKey] = clueKeyAllowedAnswersDict[nonCrossingClueKey]   
            
        for clueKey in clueKeyStillAllowedAnswersDict.keys():
            if clueKey in cluesThatIntersect:
                allowedAnswers = clueKeyStillAllowedAnswersDict[clueKey]
                guess = allowedAnswers[0][0]
                conf = allowedAnswers[0][1]
                self.solveCornerRecursive(clueKey, guess, conf, curPuzzle, corner,
                                          clueKeyStillAllowedAnswersDict, visitedPuzzles, possibleSolutions)
    
    def mergeAll(self, cornerSolutions, ignoreLongClues = True):
        '''
        cornerSolutions shall be a list of lists, where each internal list
        is a list of puzzles, sorted by puzzle confidence.
        '''
        if len(cornerSolutions) == 0:
            return None
        cur = cornerSolutions[0][0]
        for i in xrange(1, len(cornerSolutions)):
            next = cornerSolutions[i][0]
            if len(next.solvedClues) == 1 and ignoreLongClues:
                continue
            cur = cur.newMerge(next)
        return cur

    def findCorners(self, desiredCornerSize = 8, maxAnswerLength = 10):
        '''Finds regions for the decider algorithm.'''

        '''
        ########## UTILITY FUNCTIONS ###########
        '''
        def houseOrphans(reqCrosses, cornerList, corner = None, ignoreLong = True):
            '''Tries to find homes for cornerless clues.'''
            unclaim = set(self.puzzle.clueDict.keys())
            for cornered in cornerList:
                #print corner
                unclaim = unclaim - cornered
            lonelyClues = [key for key in unclaim if len(self.puzzle.clueDict[key]) <= maxAnswerLength]
            if not corner: print 'lonely clues before: ', len(lonelyClues), ' ==> ', lonelyClues
            
            lonelyDict = {}
            for lonelyClue in lonelyClues:
                if not ignoreLong or len(self.puzzle.clueDict[lonelyClue]) < maxAnswerLength:
                    maxCrosses = 0
                    bestCorner = 0
                    crosses = self.puzzle.crossesDict[lonelyClue]
                    if not corner:
                        for j in range(len(cornerList)):
                            cornerCollisions = 0
                            for clue in cornerList[j]:
                                if clue in crosses:
                                    cornerCollisions += 1
                            if cornerCollisions > maxCrosses and len(cornerList[j]) < maxCornerSize: #tweakable, avoids overloading corners
                                maxCrosses = cornerCollisions
                                bestCorner = j
                        lonelyDict[lonelyClue] = (maxCrosses, bestCorner)
                    else:
                        cornerCollisions = 0
                        for clue in corner:
                            if clue in crosses:
                                cornerCollisions += 1
                        myCorner = set(list(corner)[:])
                        if cornerCollisions > reqCrosses and len(corner) < maxCornerSize: #corner overload prevention
                            corner.add(lonelyClue)
            if corner: return myCorner
            lonelyList = sorted(lonelyDict.items(), key=lambda x: x[1][0])
            lonelyList.reverse()
            #print '######LonelyList for consideration: ', lonelyList
            unclaim = set(self.puzzle.clueDict.keys())
            for cornered in cornerList:
                unclaim = unclaim - cornered
            lonely = set([key for key in unclaim if len(self.puzzle.clueDict[key]) <= maxAnswerLength])
            i = 0
            while i < len(lonelyList): # required maxCross value
                if lonelyList[i][1][0] >= reqCrosses:
                    success = moveClue(lonelyList[i][0], lonely, cornerList[lonelyList[i][1][1]])
                    if not success: print 'error moving clue'
                    else: print '   orphan finder moved clue ', lonelyList[i][0], ' giving target new len ', len(cornerList[lonelyList[i][1][1]])
                i += 1
            
            return cornerList

        def clueCrosses(clue, corner, crossReq = 1):
            '''Returns a boolean indicating whether the clue crosses the specified corner crossReq times.'''
            crossCount = 0
            for cornerClue in corner:
                if cornerClue in self.puzzle.crossesDict[clue]:
                    crossCount += 1
            if crossCount >= crossReq: return True
            return False

        def clueAbuts(clue, corner, distance = 3):
            '''Returns true if the clue is nice and cozy with the corner (not necessarily touching it)'''
            for otherClue in corner:
                otherDir = '0'
                for c in otherClue:
                    if c.isalpha():
                        otherDir = c
                clueDir = '1'
                for c in clue:
                    if c.isalpha():
                        clueDir = c
                if clueDir == otherDir:
                    rowDist = self.puzzle.clueDict[otherClue].row - self.puzzle.clueDict[clue].row
                    colDist = self.puzzle.clueDict[otherClue].col - self.puzzle.clueDict[clue].col
                    if rowDist <= distance and rowDist >= -distance:
                        if colDist == 0:
                            #print clue, ' ABUTS CORNER: ', corner
                            return True
                    if colDist <= distance and colDist >= -distance:
                        if rowDist == 0:
                            #print clue, ' ABUTS CORNER: ', corner
                            return True
            return False

        def moveClue(clue, source, targetCorner):
            '''Attempts to move a clue from one set to another, or returns False if the request is in error.'''
            if clue not in source or len(targetCorner) >= maxCornerSize:
                return False
            targetCorner.add(clue)
            source.remove(clue)
            return len(lonely)
        
        def incCursor(cursor, origin):
            '''Moves the geographic clue finding cursor.'''
            #print 'incrementing', origin
            if origin == 'TL':
                #increment top left cursor
                if cursor[0] == self.puzzle.width - 1 and cursor[1] == self.puzzle.height - 1: return False
                elif cursor[0] < 1:
                    cursor[0] = cursor[1] + 1
                    cursor[1] = 0
                else:
                    cursor[0] -= 1
                    cursor[1] += 1
            elif origin == 'TR':
                #increment top right cursor
                if cursor[0] == 0 and cursor[1] == self.puzzle.height - 1: return False
                elif cursor[1] < 1:
                    cursor[1] = (self.puzzle.height - 1) - cursor[0] + 1
                    cursor[0] = self.puzzle.width - 1
                else:
                    cursor[0] -= 1
                    cursor[1] -= 1
            elif origin == 'BL':
                #increment bottom left cursor
                if cursor[0] == self.puzzle.width - 1 and cursor[1] == 0: return False
                elif cursor[1] >= self.puzzle.height - 1:
                    cursor[1] = (self.puzzle.height - 1) - cursor[0] - 1
                    cursor[0] = 0
                else:
                    cursor[0] += 1
                    cursor[1] += 1
            if origin == 'BR':
                #increment bottom right cursor
                if cursor[0] == 0 and cursor[1] == 0: return False
                elif cursor[0] >= self.puzzle.width - 1:
                    cursor[0] = cursor[1] - 1
                    cursor[1] = self.puzzle.height - 1
                else:
                    cursor[0] += 1
                    cursor[1] -= 1
            return True

        def cursorPos(cursor):
            '''Returns a cursor's position as a tuple for use in the coordsToNumDict'''
            return (cursor[0], cursor[1])

        def checkCorner(theCorner, allTheCorners):
            '''Appends the corner to the legit corner list and resets it if appropriate.'''
            if len(theCorner) >= maxCornerSize:
                allTheCorners.append(theCorner)
                #print 'corner reset needed!!'
                return set()
            else:
                return theCorner
        '''
        ######################### METHOD BODY ###############################################################
        #########################             ##################################
        '''

        maxCornerSize = desiredCornerSize - 2

        '''
        Identify clues
        '''
        unclaimed = set(self.puzzle.clueDict.keys())
        lonely = set([key for key in unclaimed if len(self.puzzle.clueDict[key]) <= maxAnswerLength])
        tLCursor = [0, 0]
        tRCursor = [self.puzzle.width - 1, 0]
        bLCursor = [0, self.puzzle.height - 1]
        bRCursor = [self.puzzle.width - 1, self.puzzle.height - 1]

        topLeftOrder = []
        topRightOrder = []
        bottomLeftOrder = []
        bottomRightOrder = []

        '''
        GENERATE CLUE ORDERING
        '''
        cont = True
        while cont:
            curCursor = tLCursor
            curOrder = topLeftOrder
            while curCursor :
                if curCursor[0] < 0 or curCursor[1] < 0 or curCursor[0] >= self.puzzle.width or curCursor[1] >= self.puzzle.height:
                    cont = False
                    break
                if curCursor[0] == self.puzzle.height / 2 and curCursor[1] == self.puzzle.height / 2:
                    cont = False
                    break
                #print "examining ", curCursor
                if cursorPos(curCursor) in self.puzzle.coordsToNumDict:
                    clueNum = self.puzzle.coordsToNumDict[cursorPos(curCursor)]
                    #print "found #", clueNum
                    clueA = str(clueNum) + 'A'
                    if clueA in self.puzzle.clueDict:
                        #aOK = (clueA not in topLeftOrder and clueA not in topRightOrder and clueA not in bottomLeftOrder and clueA not in bottomRightOrder)
                        aOK = True
                        if aOK:
                            #print "appending ", clueA
                            curOrder.append(clueA)
                    clueD = str(clueNum) + 'D'
                    if clueD in self.puzzle.clueDict:
                        #dOK = (clueD not in topLeftOrder and clueD not in topRightOrder and clueD not in bottomLeftOrder and clueD not in bottomRightOrder)
                        dOK = True
                        if dOK:
                            #print "appending ", clueD
                            curOrder.append(clueD)
                if curCursor == tLCursor:
                    curCursor = tRCursor
                    curOrder = topRightOrder
                elif curCursor == tRCursor:
                    curCursor = bLCursor
                    curOrder = bottomLeftOrder
                elif curCursor == bLCursor:
                    curCursor = bRCursor
                    curOrder = bottomRightOrder
                elif curCursor == bRCursor:
                    curCursor = False
            cont = incCursor(tLCursor, 'TL') and incCursor(tRCursor, 'TR') and incCursor(bLCursor, 'BL') and incCursor(bRCursor, 'BR')
            #print

        #unordered = set([key for key in unclaimed if len(self.puzzle.clueDict[key]) <= maxAnswerLength])
        #unordered = unordered - set(topLeftOrder) - set(topRightOrder) - set(bottomRightOrder) - set(bottomLeftOrder)
        #print unordered, '====\n'

        '''             333333333333333333333333333333333333333333333333333333333
        333333333333333333###=== Find corners using the ordering ===###33333333333333333333333333
                 333333333333333333333333333333333333333333333333333333333'''
        corners = []
        realCorners = []
        for j in range(4):
            corners.append(set())

        orderings = [topLeftOrder, topRightOrder, bottomLeftOrder, bottomRightOrder]
        i = 0
        ''' TODO: Optimize number of crosses instead of just requiring a small number!! ==================================     ======      ======|||||=====         =====||||||\/\/\/\/\||||||=====
        '''
        while i < len(topLeftOrder) * 4:
            currOrder = orderings[i % 4]
            for j in range(len(corners)):
                corner = set(list(corners[j])[:])
                print 'working with corner: ', corner, 'ordering: ', currOrder[:10]
                if i / 4 < len(currOrder) and currOrder[i / 4] in lonely and\
                        (\
                            (clueCrosses(currOrder[i / 4], corner, 2))\
                            or (clueCrosses(currOrder[i / 4], corner, 1) and (len(realCorners) == 0 or (clueAbuts(currOrder[i / 4], corner))))\
                            or len(corner) < 1\
                            or (clueAbuts(currOrder[i / 4], corner) and len(corner) < 5) and i < 13\
                        ):
                    success = moveClue(currOrder[i / 4], lonely, corner)
                    if success: print 'moved clue ', currOrder[i / 4], ' to corner ', corner
                    else: print 'failed move of clue ', currOrder[i / 4], ' to corner ', corner
                    corner = checkCorner(corner, realCorners)
                corners[j] = set(list(corner)[:])
            i += 1

        '''straighten out some corner data'''
        for realCorner in realCorners:
            if realCorner not in corners:
                corners.append(realCorner)
        n = 0
        while n < len(corners):
            if len(corners[n]) < 4:
                corners.pop(n)
            else: n += 1

        '''first adoption drive'''
        #print 'corners after just the alg:'
        #for corner in corners:
        #    print corner

        corners = houseOrphans(2, corners)
        
        '''
        ####### Try to glom together isolated groups of orphans ########
        '''
        
        unclaim = set(self.puzzle.clueDict.keys())
        for corner in corners:
            unclaim = unclaim - corner
        lonely = sorted([key for key in unclaim if len(self.puzzle.clueDict[key]) <= maxAnswerLength], key = lambda x: len(self.puzzle.clueDict[x]))
        
        i = -1
        j = 0
        while (i != len(lonely) or cont) and j < len(lonely):
            i = len(lonely)
            lonelyClue = lonely[j]
            #print 'Checking glom potench of clue ', lonelyClue, '  len(lonely) before is ', len(lonely)
            cont = True
            crosses = self.puzzle.crossesDict[lonelyClue]
            for cross in crosses:
                if cont and cross in lonely:
                    crossesCrosses = self.puzzle.crossesDict[cross]
                    for crossesCross in crossesCrosses:
                        corner3 = set([lonelyClue, cross])
                        if cont and crossesCross in lonely and crossesCross != lonelyClue and clueAbuts(crossesCross, corner3):
                            #may as well add to the group
                            corner3.add(crossesCross)
                            unclaimed = set(self.puzzle.clueDict.keys())
                            for corner in corners:
                                unclaimed = unclaimed - corner
                            unclaimed = unclaimed - corner3
                            lonely = [key for key in unclaimed if len(self.puzzle.clueDict[key]) <= maxAnswerLength]
                            corner3 = houseOrphans(2, corners, corner3)

                            #print 'formed family ', corner3
                            corners.append(corner3)
                            unclaim = set(self.puzzle.clueDict.keys())
                            for corner in corners:
                                unclaim = unclaim - corner
                            lonely = [key for key in unclaim if len(self.puzzle.clueDict[key]) <= maxAnswerLength]
                            cont = False
                            #print 'len(lonely) after: ', len(lonely)
                            j = 0
            if cont:
                j += 1

        
        longCorners = [c for c in corners if len(c) >= maxCornerSize / 2]
        shortCorners = [c for c in corners if len(c) < maxCornerSize / 2]
        shortCorners = houseOrphans(2, shortCorners)

        maxCornerSize = desiredCornerSize

        corners = shortCorners
        ##########################
        # COMBINE COMPATIBLE SHORT LENGTH REGIONS
        
        combine = {}
        for i in range(len(corners)):
            if i not in combine.keys() and i not in combine.values():
                collList = [0 for k in range(len(corners))]
                for j in range(len(corners)):
                    if j != i and j not in combine.keys() and j not in combine.values()\
                            and len(corners[i]) + len(corners[j]) <= maxCornerSize:
                        for eachClue in corners[i]:
                            for otherClue in corners[j]:
                                if otherClue in self.puzzle.crossesDict[eachClue]:
                                    collList[j] += 1
                maxCx = 0
                mIndex = 0
                for jj in range(len(corners)):
                    if collList[jj] > maxCx\
                            and jj not in combine.keys() and jj not in combine.values():
                        maxCx = collList[jj]
                        mIndex = jj
                if maxCx >= 2: #control the number of clue crosses required. Right hand value MUST BE AT LEAST ZERO
                    #print 'We have a winner: indices ', i, ' and ', mIndex, ' will fit'
                    combine[i] = mIndex
        #print '==combine==\n', combine.keys(), '\n', combine.values(), '===\n'
        for key in combine.keys():
            source = corners[key]
            target = corners[combine[key]]
            #print 'combining ', source, ' with ', target
            for clue in list(source)[:]:
                success = moveClue(clue, source, target)
                if not success:
                    print "Error combining corners!!"
        
        #get rid of some corners
        m = 0
        while m < len(corners):
            if len(corners[m]) < 4:
                corners.pop(m)
            else:
                m += 1

        #clean up
        corners = corners + longCorners

        maxCornerSize = desiredCornerSize - 2
        '''
        ###############
        extend corners
        '''

        for z in range(3):
            corners = houseOrphans(1, corners)


        maxCornerSize = desiredCornerSize
        '''Transfer some clues'''
        for w in range(2):
            newLens = []
            for c in corners:
                newLens.append(len(c))
            p = 0
            while p < len(corners):
                corner = corners[p]
                moveDict = {}
                for clue in corner:
                    print "examining ", clue
                    if clue == '32A':
                        print "Holla!!"
                    crossCount = 0
                    for otherClue in list(corner - set([clue])):
                        if clue in self.puzzle.crossesDict[otherClue]:
                            crossCount += 1
                    if crossCount == 1:
                        maxCross = 1
                        tar = None
                        for q in range(0, p) + range(p + 1, len(corners)):
                            otherCorner = corners[q]
                            i = 0
                            while clueCrosses(clue, otherCorner, i + 1):
                                i += 1
                            if i > maxCross and newLens[q] < maxCornerSize:
                                maxCross = i
                                print clue, ' CAN GO IN ', otherCorner
                                tar = q
                        if tar:
                            moveDict[clue] = (p, tar)
                            newLens[p] -= 1
                            newLens[tar] += 1
                for clue in moveDict.keys():
                    print 'Rerelegating ', clue, ' from ', corners[moveDict[clue][0]], ' to ', corners[moveDict[clue][1]]
                    success = moveClue(clue, corners[moveDict[clue][0]], corners[moveDict[clue][1]])
                    if success: 'Successful transfer'
                    else: print ' failed transfer'
                p += 1

        for z in range(3):
            corners = houseOrphans(1, corners)

        '''
        #### This is why we can't have nice things. #####
        '''
        unclaim = set(self.puzzle.clueDict.keys())
        for corner in corners:
            unclaim = unclaim - corner
        lonely = [key for key in unclaim if len(self.puzzle.clueDict[key]) <= maxAnswerLength]
        print 'len(lonely): ', len(lonely)
        print 'Lonely keys: ', lonely, '\n=============\n'
        
        '''Corner statistics'''
        tL = 0
        mnC = len(corners[0])
        mxC = len(corners[0])
        for corner in corners:
            leng = len(corner)
            tL += leng
            if leng > mxC: mxC = leng
            if leng < mnC: mnC = leng
        print '~~~\nNumber of corners without long clues: ', len(corners), '  Average size: ', float(tL) / float(len(corners))
        print '    shortest: ', mnC, '  longest: ', mxC, '\n~~~'

        '''Make corners for the long clues'''
        for key in self.puzzle.clueDict.keys():
            if len(self.puzzle.clueDict[key]) > maxAnswerLength:
                newCorner = set([key])
                corners.append(newCorner)

        return corners
    
    def printWhoSolvedWhat(self, puzzle):
        '''
        Prints a key of who is responsible for returning which answers in a given puzzle.
        '''
        for clueKey in puzzle.solvedClues:
            curAnswer = puzzle.getClueFill(clueKey)
            perpetrators = self.answerToSolversDict[(clueKey, curAnswer)]
            print clueKey, curAnswer, puzzle.confDict[clueKey]
            print puzzle.clueDict[clueKey]
            print perpetrators
            print
            
    def answerInClue(self, answer, clueKey):
        '''
        Checks to see if the answer provide is contained within the clue.
        We can disregard the answer if this is the case.
        '''
        clue = self.puzzle.clueDict[clueKey].clue.lower()
        return answer.lower() in clue.split()
            
    def printCorner(self, corner):
        '''
        Provides a way to print out a corner that shows whether each cell is involved
        in one or two clues in the corner
        '''
        clone = self.puzzle.clone()
        for clueKey in corner:
            clueObj = self.puzzle.clueDict[clueKey]
            row  = clueObj.row
            col = clueObj.col
            dir = clueObj.dir
            if dir == "ACROSS":
                for i in range(len(clueObj)):
                    if clone.fill[row][col] == "-":
                        clone.fill[row][col] = "x"
                    elif clone.fill[row][col] == "x":
                        clone.fill[row][col] = "Y"
                    col+=1
            elif dir == "DOWN":
                for i in range(len(clueObj)):
                    if clone.fill[row][col] == "-":
                        clone.fill[row][col] = "x"
                    elif clone.fill[row][col] == "x":
                        clone.fill[row][col] = "Y"
                    row+=1
        print clone
        
def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--modName", type=str) #from mainMain.py
    parser.add_argument("-g", "--gui", action="store_true")
    parser.add_argument("-r", "--resolver", action="store_true")
    parser.add_argument("--gen", action="store_true")
    parser.add_argument("--test", action="store_true")
    parser.add_argument("--read", action="store_true")
    parser.add_argument("--write", action="store_true")
    parser.add_argument("--both", action="store_true")
    parser.add_argument("--bothFolder", action="store_true")
    parser.add_argument("fileName", type=str)
    args = parser.parse_args()
    
    if args.test:
        fileName = args.fileName
        print fileName
        if args.read:
            answer = test(fileName, True)
        elif args.write:
            answer = test(fileName, False)
        else:
            print "usage error, try --help for help"
        if args.resolver:
            resolver = Resolver(answer)
            answer = resolver.bestPuzzle()
        if args.gui:
            answer.tkShow()
        else:
            print answer
            print answer.score()
    elif args.gen:
        folder = args.fileName
        genAllFlatFiles(folder)
    elif args.both:
        answer = runWholePipeline(args.fileName)
        if args.gui:
            answer.tkShow()
        else:
            print answer
            print answer.score()
    elif args.bothFolder:
        runWholePipelineOnFolder(args.fileName)
    else:
        print "must include either --test or --gen"
    
def test(puzzleName, read):
    puzz = AWPuzzle(puzzleName)
    decider = Decider(puzz, True)
    puzz.printSolution()
    solvedPuzz = decider.solvePuzzle(read) #true means read from corner flat file. False means generate corner flat file
    return solvedPuzz

def genAllFlatFiles(folder):
    import os
    shortPuzzleNames = os.listdir(folder)
    fullPaths = [folder + "/" + fileName for fileName in shortPuzzleNames]
    print fullPaths
    for path in fullPaths:
        if path[-3:] != "puz":
            continue
        print
        print "TRYIN", path
        
        try:
            puzz = AWPuzzle(path)
            Decider(puzz, False)
            puzz.printKeyToFile()
        except Exception as e:
            print e
            print "couldnt get it working for puzzle", path

def runWholePipeline(fileName):
    import time
    start = time.time()
    puzz = AWPuzzle(fileName)
    decider = Decider(puzz, False)
    solvedPuzz = decider.solvePuzzle(False)
    resolver = Resolver(solvedPuzz)
    solvedPuzz = resolver.bestPuzzle()
    end = time.time()
    print "RUNTIME =", end-start, "Seconds!"
    
    with open("FullSolutions/" + puzz.name + ".sol", "w") as f:
        pickle.dump(solvedPuzz, f)
    return solvedPuzz

def runWholePipelineOnFolder(folder):
    import os
    shortPuzzleNames = os.listdir(folder)
    fullPaths = [folder + "/" + fileName for fileName in shortPuzzleNames]
    print fullPaths
    for path in fullPaths:
        if path[-3:] != "puz":
            continue
        print
        print "TRYIN", path
        
        try:
            puzz = AWPuzzle(path)
            decider = Decider(puzz, False)
            puzz.printKeyToFile()
            solvedPuzz = decider.solvePuzzle(False)
            resolver = Resolver(solvedPuzz)
            solvedPuzz = resolver.bestPuzzle()
            with open("FullSolutions/" + puzz.name + ".sol", "w") as f:
                pickle.dump(solvedPuzz, f)
        except Exception as e:
            print e
            print "couldnt get it working for puzzle", path
    
if __name__ == "__main__":
    main()
