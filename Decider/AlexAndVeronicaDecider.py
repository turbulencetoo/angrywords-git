#AlexAndVeronicaDecider.py

from DataStructure.dataStructure import *
import pickle
import re
import copy


class AlexAndVeronicaDecider(object):
    """
    Tries to solve a puzzle by filling in the highest confidence answer, then next highest, and so on. 
    Backtracks if putting in a clue makes too many crosses in the other direction unsolveable.
    Region-independent.
    """
    
    def __init__(self, puzzle):
        '''
        Param: puzzle
        Should be instance of the AWPuzzle class
        '''
        self.puzzle = puzzle
        self.clueNumAnswersDict = {}
        
        with open("FlatFiles/"+self.puzzle.name+".answers") as f:
            self.clueNumAnswersDict = pickle.load(f)
                        
    def getBestClue(self, unsolvedClues):
        """
        Given a list of unsolved clues, returns the clue with the highest-confidence answer, as well as that answer and its confidence
        """
        
        bestClue = ''
        bestAnswer = ''
        confidence = 0.0
        for clue in unsolvedClues:
            if len(self.currentGuesses[clue]) > 0:
                possibleAnswer = self.currentGuesses[clue][0]
                if possibleAnswer[1] > confidence:
                    bestClue = clue
                    bestAnswer = possibleAnswer[0]
                    confidence = possibleAnswer[1]
        return bestClue, bestAnswer, confidence
    
    def answerInClue(self, answer, clue):
        """Super basic check to see if a suggested answer is a word"""
        clue = self.puzzle.clueDict[clue].clue.lower()
        return answer.lower() in clue.split()
    
    def solvePuzzle(self):
        """Solves the puzzle as best it can"""
    
        unsolvedClues = self.clueNumAnswersDict.keys()
        self.currentGuesses = copy.deepcopy(self.clueNumAnswersDict)
        
        
        prevClue = None
        loops = 0
        while unsolvedClues:
            curClue, bestAnswer, confidence = self.getBestClue(unsolvedClues)   # Choose best answer to fill in
            
            # If the puzzle is completely full or we have no more answers, return
            if not curClue:
                return self.puzzle
                
            newPuz = self.puzzle.clone()
            
            if self.answerInClue(bestAnswer, curClue):
                success = False
            else:
                success = newPuz.fillClue(curClue, bestAnswer)      # Attempt to fill in the answer
            if success:
                prunedratio = self.checkCrossedClues(curClue)
                
                #prunedratio = 1 if we prune all answers
                if prunedratio>.99 and prunedratio!=1:
                    #Removes the bad answer to the clue
                    oldGuesses = self.currentGuesses[curClue]
                    oldGuesses.remove((bestAnswer, confidence))
                    self.currentGuesses[curClue] = oldGuesses
                    #Removes the clue if it doesn't have any answers left
                    if len(oldGuesses) == 0:
                        unsolvedClues.remove(curClue)
                #If the answer is good, clue is solved
                else:
                    newPuz.confDict[curClue] = confidence
                    self.puzzle = newPuz
                    unsolvedClues.remove(curClue)
                    self.updateConf(curClue)
                    
            #If answer did not fit in current fill
            else:
                #Remove the answer from the list of possible answers
                oldGuesses = self.currentGuesses[curClue]
                oldGuesses.remove((bestAnswer, confidence))
                self.currentGuesses[curClue] = oldGuesses
                if len(oldGuesses) == 0:
                    unsolvedClues.remove(curClue)
            #If it tries the same clue more than 10 times in a row, it gives up on that clue
            if prevClue == curClue:
                loops += 1
            else:
                prevClue = curClue
                loops = 0
            if loops >= 10 and curClue in unsolvedClues:
                unsolvedClues.remove(curClue)
            
        return self.puzzle
     
    def checkCrossedClues(self, clue):
        '''
        Checks the possible answers for all the clues that cross 'clue' assuming clue has been filled in by some value
        Return the ratio of number of crossed clues with possible solutions after pruning over the total number of crossed clues
        Also returns the total number of clues pruned by filling in the original clue
        '''
        crossedClues = self.puzzle.crossesDict[clue]
        totalPruned = 0.0
        totalGuesses = 0.0
        totalCrosses = len(crossedClues)
        for crossedClue in crossedClues:
            clueGuesses = self.currentGuesses[crossedClue]
            originalLength = len(clueGuesses)
            possibleAnswers = self.pruneClues(clueGuesses, self.puzzle.getClueFill(crossedClue))
            totalPruned += originalLength-len(possibleAnswers)
            totalGuesses += originalLength
        return totalPruned/totalGuesses
            
    def pruneClues(self, clueList, currentFill):
        '''
        This returns the possible answers for a clue given a fill
        '''
        currentFill = currentFill.lower()
        currentFill = currentFill.replace("-", "[a-zA-Z]")
        possibleAnswers = []
        for guess, confidence in clueList:
            match = re.match(currentFill, guess.lower())
            if match:
                possibleAnswers.append((guess, confidence))
        return possibleAnswers
        
    def recalcConf(self, oldClueList, newClueList):
        """
        If guesses for a given clue have been eliminated, increase the confidences of those guesses that remain and are still 
        consistent with the current fill
        """
    
        removedGuesses = list(set(oldClueList).difference(set(newClueList)))
        confidenceRemoved = 0
        for guess in removedGuesses:
            confidenceRemoved += guess[1]
        amountToAdd = confidenceRemoved/len(newClueList)
        newGuessList = []
        for item in newClueList:
            newGuessList.append((item[0],item[1]+amountToAdd))
        return newGuessList
         
    def updateConf(self, clue):
        """Once a fill for a clue has been decided upon, update the confidences for all the crossing clues"""
        
        crossedClues = self.puzzle.crossesDict[clue]
        for crossedClue in crossedClues:
            oldClueGuesses = self.currentGuesses[crossedClue]
            newClueGuesses = self.pruneClues(oldClueGuesses, self.puzzle.getClueFill(crossedClue))
            if newClueGuesses:
                self.currentGuesses[crossedClue] = self.recalcConf(oldClueGuesses,newClueGuesses)
        return

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--modName", type=str)
    parser.add_argument("-r", "--resolver", action="store_true")
    parser.add_argument("-g", "--gui", action="store_true")
    parser.add_argument("fileName")
    args = parser.parse_args()
    # Solve the puzzle
    puzz = AWPuzzle(args.fileName)
    decider = AlexAndVeronicaDecider(puzz)
    decider.solvePuzzle()
    print decider.puzzle.score()
    print decider.puzzle

    # Check for optional flags

    if args.resolver:      # Use resolver
        from Resolver.resolver import Resolver
        resolver = Resolver(decider.puzzle)
        decider.puzzle = resolver.bestPuzzle()
        print
        print "------------------------------"
        print
        print decider.puzzle
        print decider.puzzle.score()
    if args.gui:      # Use GUI
        decider.puzzle.tkShow()       
    
if __name__ == "__main__":
    main()
