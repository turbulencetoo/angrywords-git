from puz import *
import re
from tkShow import *

class AWPuzzle:
    def __init__(self, fileName=None):
        '''
        PARAM fileName:
        a fileName for a *.puz file
        '''
        if not fileName:
            return
        
        puzzle = read(fileName) #from third-party puz.py

        self.name = re.match("^.*/(.*)\\.puz$", fileName).group(1)
        #fileName == 'Dir1/Dir2/ab293042.puz' -> self.name == 'ab293042'
        
        self.width = puzzle.width
        self.height = puzzle.height
        self.confidence = 0
        self.solution = []
        self.fill = []
        for i in range(puzzle.height):
            solRow = []
            fillRow = []
            for j in range(puzzle.width):
                solRow.append(puzzle.solution[i * puzzle.height + j])
                fillRow.append(puzzle.fill[i * puzzle.height + j])
                
            self.solution.append(solRow)
            self.fill.append(fillRow)
        
        self.parent = None

        self.coordsToNumDict = {} #2-ple[int] -> int
        #maps a coordinate tuple to the number printed on that square.
        #self.coordsToNumDict[(0, 0)] = 1
        #self.coordsToNumDict[(1, 2)] raises KeyError
        self.genNumberDict()
        
        self.clueDict = self.populateClueDict(puzzle) # str-> AWClue
        self.solvedClues = set([]) # set of str
        
        self.crossesDict = {} # str -> set[str]
        #maps a clueKey to a list of crossing clueKeys
        self.genCrossesDict()
         
        self.confDict = {} # str -> float
        #Maps the clueKeys of solved clues to the confidence of the clue  
        
        self.coordsToConfDict = {} # tuple[int] -> float
        #for each square, maps the coordinates to our confidence for that square. 
        #this is pretty shitty
        
    def clone(self):
        '''
        Creates an exact replica of self, with relevant aspects deep copied.
        '''
        newPuz = AWPuzzle()
        
        #deep copy the puzzle's fill
        newPuz.fill = []
        for row in self.fill:
            newPuz.fill.append(row[:])
        newPuz.solvedClues = set([])
        #deep copy the solved clues
        for clueKey in self.solvedClues:
            newPuz.solvedClues.add(clueKey)
        #deep copy the confDict
        newPuz.confDict = {}
        for key, value in self.confDict.items():
            newPuz.confDict[key] = value
        #deep copy the coords to conf dict
        newPuz.coordsToConfDict = {}
        for key, value in self.coordsToConfDict.items():
            newPuz.coordsToConfDict[key] = value
            
        #shallow copy other necessities
        newPuz.name = self.name
        newPuz.width = self.width
        newPuz.height  = self.height
        newPuz.clueDict = self.clueDict
        newPuz.solution = self.solution
        newPuz.confidence = self.confidence
        newPuz.crossesDict = self.crossesDict
        newPuz.coordsToNumDict = self.coordsToNumDict

        newPuz.parent = self

        return newPuz
                
    def __str__(self):
        return repr(self)
    
    def __repr__(self):
        strList = ["\n"]
        for row in range(self.height):
            for col in range(self.width):
                ch = self.fill[row][col]
                correctCh = self.solution[row][col]
                if ch == correctCh:
                    strList.append(ch)
                else:
                    strList.append(ch.lower())
                strList.append(" ")
            strList.append("\n")
        return "".join(strList)

    def tkShow(self):
        '''
        invokes the GUI which displays on the screen
        '''
        try:
            myGUI = TkShow(self)
            myGUI.tkShow()
        except:
            print
            print ". . D O W N"
            print ". . L . O ."
            print ". A N G R Y"
            print ". . . . D ."
            print "C O M P S ."
            print "S . . . . ."
    
    def printSolution(self):
        '''
        Prints the solved puzzle to stdout
        '''
        strList = []
        for row in self.solution:
            for ch in row:
                strList.append(ch)
                strList.append(" ")
            strList.append("\n")
        print "".join(strList)
    
    def populateClueDict(self, puzzle):
        """
        Given a puz.py Puzzle object and a solution from AWPuzzle.solution returns 
        a dict where clueKeys are mapped to clueObjects
        """
        solution = self.solution
        if not solution:
            soultion  = self.fill 
        clues = puzzle.clues[:]
        clueDict = {}
        row = 0
        col = 0
        while len(clues) > 0:
            while col < puzzle.width and solution[row][col] == '.':
                col += 1
            if col >= puzzle.width:
                #Goes to next line
                col = 0
                row += 1
            
            while col < puzzle.width and solution[row][col] == '.':
                col += 1
            curAcross = ''
            startRow = row
            startCol = col
            clueKey = str(self.coordsToNumDict[(row,col)])+'A'
            while col < puzzle.width and solution[row][col] != '.':
                 curAcross += solution[row][col]
                 col += 1
            clueDict[clueKey] = AWClue(clues.pop(0), startRow, startCol, "ACROSS", curAcross)
            newCol = col - len(curAcross)
            for i in range(len(curAcross)):
                newRow = row
                if (newRow-1) < 0 or solution[newRow-1][newCol] == ".":
                    curDown = ''
                    startRow = newRow
                    startCol = newCol
                    clueKey = str(self.coordsToNumDict[(newRow,newCol)])+'D'
                    while newRow < puzzle.height and solution[newRow][newCol] != '.':
                        curDown += solution[newRow][newCol]
                        newRow += 1
                    clueDict[clueKey] = AWClue(clues.pop(0), startRow, startCol, "DOWN", curDown)
                newCol += 1
                
        return clueDict
    
    def genNumberDict(self):
        '''
        generates the dictionary that maps coordinates (as a tuple of ints) to numbers (ints)
        For example (0,1) will be labeled 2 in many puzzles.
        '''
        self.coordsToNumDict = {}
        counter = 1
        for row in range(self.height):
            for col in range(self.width):
                if (row == 0 or col == 0) and (self.fill[row][col] == "-"):
                    self.coordsToNumDict[(row,col)] = counter
                    counter += 1
                elif (self.fill[row-1][col] == '.' or self.fill[row][col-1] == '.') and (self.fill[row][col] == "-"):
                        self.coordsToNumDict[(row,col)] = counter
                        counter += 1
                        
    def genCrossesDict(self):
        '''
        generates the dictionary mapping a clueKeys x to a set that 
        contains the clueKeys that x crosses
        '''
        for clueKey in self.clueDict.keys():
            crosses = []
            clueObj = self.clueDict[clueKey]
            length = len(clueObj)
            row = clueObj.row
            col = clueObj.col
            if clueObj.dir == "ACROSS":
                for i in range(length):
                    j = 1
                    while True:
                        if row-j < 0 or self.fill[row - j][col + i] == ".":
                            coords = (row-j +1, col + i)
                            break
                        j += 1
                    crossingClueKey = str(self.coordsToNumDict[coords]) + "D"
                    crosses.append(crossingClueKey)
            elif clueObj.dir == "DOWN":
                for i in range(length):
                    j = 1
                    while True:
                        if col-j < 0 or self.fill[row + i][col -j ] == ".":
                            coords = (row + i, col-j +1)
                            break
                        j += 1
                    crossingClueKey = str(self.coordsToNumDict[coords]) + "A"
                    crosses.append(crossingClueKey)
            else:
                raise ValueError
            self.crossesDict[clueKey] = set(crosses)
            
    def updateCoordsToConfDict(self):
        '''
        for space in the puzzle that is filled in, we assign that space a confidence
        based on the maximum of the confidences of the two answers that cross that space
        '''
        for i in range(self.height):
            for j in range(self.width):
                fill = self.fill[i][j]
                if fill != "-" and fill != ".":
                    clueKeys = self.findClueKeysFromCoords((i,j))
                    maxConf = max(self.confDict.get(clueKeys[0], -1), self.confDict.get(clueKeys[1], -1) )
                    self.coordsToConfDict[(i,j)] = maxConf
                                   
    def findClueKeysFromCoords(self, coords):
        '''
        Given a tuple of integer coordinates, returns the clueCkey of the
        across clue, and the clue key of the down clue as a list of strings
        '''
        row, col = coords
        while row >= 0 and self.fill[row][col] != ".":
            row -= 1
        downClueKey = str( self.coordsToNumDict[(row+1,col)] ) + "D"
        
        row, col = coords #reset
        while col >= 0 and self.fill[row][col] != ".":
            col -= 1
        acrossClueKey = str( self.coordsToNumDict[(row, col+1)] ) + "A"
        
        return [acrossClueKey, downClueKey]
    
    def fillClue(self, clueKey, guess):
        '''
        fills self with a given guess for a given clueKey
        updates self.sovledClues with this clueKey
        '''
        clueObj = self.clueDict[clueKey]
        length = len(clueObj)
        row  = clueObj.row
        col = clueObj.col
        dir = clueObj.dir
        if not self.answerFits(clueKey, guess):
            return False
        guess = guess.upper()
        if dir == "ACROSS":
            for char in guess:
                self.fill[row][col] = char
                col+=1
        elif dir == "DOWN":
            for char in guess:
                self.fill[row][col] = char
                row+=1
        else:
            raise ValueError, "Direction was neither 'ACROSS' nor 'DOWN'"
        self.solvedClues.add(clueKey)
        return True
        
    def answerFits(self, clueKey, guess):
        '''
        returns whether or not a given guess has no conflicts with self
        '''
        currentFill = self.getClueFill(clueKey)
        if len(guess) != len(currentFill):
            return False
        for i in xrange(len(guess)):
            if currentFill[i] == "-":
                continue
            if currentFill[i].upper() != guess[i].upper():
                return False
        return True
        
    def getClueFill(self, clueKey, solution = False):
        '''
        for a given clue key, returns as a string what is currently filled
        in the appropriate spaces. If solution == True, then we return a string
        containing the solution to the given clueKey
        '''
        clueObj = self.clueDict[clueKey]
        length = len(clueObj)
        row  = clueObj.row
        col = clueObj.col
        dir = clueObj.dir
        strList = []
        for i in range(length):
            if solution:
                strList.append(self.solution[row][col])
            else:
                strList.append(self.fill[row][col])
            if dir == "ACROSS":
                col += 1
            elif dir == "DOWN":
                row += 1
        return "".join(strList).upper()

    def sharesALetterWith(self, other):
        '''
        Returns true if there is a cell that both puzzles have filled in.
        Returns false if the sets of cells filled in by puzzles are disjoint
        '''
        for row in xrange(len(self.fill)):
            for col in xrange(len(self.fill[row])):
                if self.fill[row][col] != '-' and \
                self.fill[row][col] != '.' and \
                other.fill[row][col] != '-' and \
                other.fill[row][col] != '.':
                    return True
        return False
            
    def newMerge(self, other):
        '''
        Returns a new puzzle that combines self with other
        letter conflicts are overwritten by the letter with the highest confidence
        '''
        clone = self.clone()
        for row in xrange(len(clone.fill)):
            for col in xrange(len(clone.fill[row])):
                if other.fill[row][col].lower() == "-":
                    pass #keep clone fill
                elif clone.fill[row][col].lower() == "-":
                    clone.fill[row][col] = other.fill[row][col]
                    clone.coordsToConfDict[(row,col)] = other.coordsToConfDict[(row,col)]
                elif clone.fill[row][col].lower() != other.fill[row][col].lower():
                    cloneConf = clone.coordsToConfDict[(row,col)]
                    otherConf = other.coordsToConfDict[(row,col)]
                    if otherConf > cloneConf:
                        clone.fill[row][col] = other.fill[row][col]
                        clone.coordsToConfDict[(row,col)] = other.coordsToConfDict[(row,col)]
                    else:
                        pass #keep clone fill
                else:
                    pass
                    #puzzles had same fill, yay
                
        for clueKey in clone.clueDict.keys():
            clone.solvedClues = set([])
            if "-" not in clone.getClueFill(clueKey):
                clone.solvedClues.add(clueKey)
                
        return clone
                    
    def __hash__(self):
        clues = sorted(self.solvedClues, cmp = clueSort)
        return hash(str(self.fill) + str(clues))

    def __eq__(self, other):
        return hash(self) == hash(other)
    
    def score(self):
        '''
        returns the number of letters answered correctly and the total number of letters
        '''
        score = 0
        total = 0
        for i in range(len(self.fill)):
            for j in range(len(self.fill[i])):
                if self.fill[i][j].lower() != '.':
                    total +=1
                    if self.fill[i][j].lower() == self.solution[i][j].lower():
                        score += 1
        return score, total
      
    def printKeyToFile(self):
        '''
        utility that prints a handout to a file
        '''             
        fileName = "Keys/" + self.name + ".key"
        with open(fileName, "w") as f:
            clueKeys = self.clueDict.keys()
            clueKeys = sorted(clueKeys, cmp=clueSort)
            for clueKey in clueKeys:
                clueObj = self.clueDict[clueKey]
                clue = clueObj.clue
                answer = clueObj.answer.upper()
                coords = (clueObj.row, clueObj.col)
                f.write("%3s %8s %s\n" %(clueKey, coords, answer))
                f.write("%s\n" % (clue.encode("ascii", "ignore")))
                f.write("\n")
                
def clueSort(key1, key2):
    '''
    Helper function for AWPuzzle.printKeyToFile,
                        AWPuzzle.__hash__
    '''
    if "A" in key1 and "D" in key2:
        return -1
    elif "D" in key1 and "A" in key2:
        return 1
    else:
        key1 = int(key1[:-1])
        key2 = int(key2[:-1])
        return -1 if key1 < key2 else 1
    
class AWClue:
    '''
    Holds information about a clue in the puzzle
    '''
    def __init__(self, clue, row, col, dir, answer):
        self.clue = clue
        self.row = row
        self.col = col
        self.dir = dir
        self.answer = answer
    def __len__(self):
        return len(self.answer)
    def __unicode__(self):
        return unicode(self.clue)
    
    def __repr__(self):
        return str(self.clue.encode("UTF-8"))
    def __str__(self):
        return repr(self)

class Answers:
    '''
    Almost worthless. Should be a tuple.
    '''
    def __init__(self, answers, confidence):
        self.answers = answers
        self.answers.sort(key=lambda x: x[1])
        self.answers.reverse()
        self.confidence = confidence
        
    def getAnswers(self):
        return self.answers
        
    def getConfidence(self):
        return self.confidence
    
    def __getitem__(self, index):
        if index == 0:
            return self.answers
        elif index == 1:
            return self.confidence
        else:
            raise IndexError("<class: Answers> has only 2 fields")
 
def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--modName", type=str) #from mainMain.py
    parser.add_argument("fileName", type=str)
    parser.add_argument("-g", "--gui", action="store_true")
    args = parser.parse_args()
    
    fileName = args.fileName
    ourPuz = AWPuzzle(fileName)
    ourPuz.printKeyToFile()
    if args.gui:
        newPuz = ourPuz.clone()
        for clue in sorted(ourPuz.clueDict.keys(), key = lambda x: int(''.join(c for c in x if c.isdigit()))):
            answer = ourPuz.getClueFill(clue, True)
            newPuz.fillClue(clue, answer)
            newPuz = newPuz.clone()
        newPuz.fill = newPuz.solution
        newPuz.tkShow()
    else:
        print "Usage: python mainMain.py DataStructure.dataStructure filename [g]"
    
if __name__ == "__main__":
    main()