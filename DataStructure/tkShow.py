#tkShow.py

from Tkinter import *
import time

class TkShow:
    '''
    Main graphical interface class for representing puzzle solution backtrace
    '''
    def __init__(self, puzz):
        self.root = Tk()
        self.root.title("Angry Words")
        #UI elements
        self.labels = []
        self.clueInfo = None
        self.trace = []
        self.prevButton = None
        self.nextButton = None
        self.across = None
        self.down = None

        self.puzzle = puzz
        self.currSel = None

        self.alive = False
        self.tkShow()

    def drawInterface(self):
        '''Initializes static UI elements -- a one-off process
        '''
        #navigation buttons
        pB = Button(self.root, text = 'PREVIOUS', activebackground = 'darkgray', bg = 'lightgray', command = self.previous, overrelief = FLAT)
        pB.grid(row = 0, column = self.puzzle.width + 1, columnspan = 2, sticky = E + W)
        nB = Button(self.root, text = 'NEXT', activebackground = 'darkgray', bg = 'lightgray', command = self.next, overrelief = FLAT)
        nB.grid(row = 0, column = self.puzzle.width + 3, columnspan = 2, sticky = E + W)
        self.prevButton = pB
        self.nextButton = nB
        aB = Button(self.root, text = 'ANIMATE', activebackground = 'darkgray', bg = 'lightgray', command = self.animate, overrelief = FLAT)
        aB.grid(row = 1, column = self.puzzle.width + 1, columnspan = 4, sticky = E + W)

        #clue list boxes, scrollbars, and labels        
        aLabel = Label(self.root, text = 'ACROSS')
        dLabel = Label(self.root, text = 'DOWN')
        aLabel.grid(row = self.puzzle.height + 1, column = self.puzzle.width + 1, columnspan = 2)
        dLabel.grid(row = self.puzzle.height + 1, column = self.puzzle.width + 3, columnspan = 2)

        aScrollbar = Scrollbar(self.root, orient = VERTICAL)
        self.across = Listbox(self.root, selectmode = SINGLE, yscrollcommand = aScrollbar.set, width = 40)
        aScrollbar.config(command = self.across.yview)
        self.across.bind('<<ListboxSelect>>', self.select)

        dScrollbar = Scrollbar(self.root, orient = VERTICAL)
        self.down = Listbox(self.root, selectmode = SINGLE, yscrollcommand = dScrollbar.set, width = 40)
        dScrollbar.config(command = self.down.yview)
        self.down.bind('<<ListboxSelect>>', self.select)

        for key in sorted(self.puzzle.clueDict.keys(), key = lambda x: int(x[:len(x) - 1])):
            if 'A' in key:
                self.across.insert(END, key + ' ' + self.puzzle.clueDict[key].clue)
            elif 'D' in key:
                self.down.insert(END, key + ' ' + self.puzzle.clueDict[key].clue)

        self.across.grid(row = self.puzzle.height + 2, column = self.puzzle.width + 1)
        aScrollbar.grid(row = self.puzzle.height + 2, column = self.puzzle.width + 2, sticky = NSEW)
        self.down.grid(row = self.puzzle.height + 2, column = self.puzzle.width + 3)
        dScrollbar.grid(row = self.puzzle.height + 2, column = self.puzzle.width + 4, sticky = NSEW)

        #clue info box
        labelfont = ('courier', 14, 'bold')
        self.clueInfo = Label(self.root, font = labelfont, width = 50, justify = LEFT, anchor = W,\
                                bg = 'lightgray', text = 'Clue: \nCurrent fill: \nSolution: \n', padx = 10, pady = 10)
        self.clueInfo.grid(row = 2, rowspan = self.puzzle.height - 2, column = self.puzzle.width + 1, columnspan = 4, sticky = NSEW)

        self.root.bind("<Key>", self.keyPress)

    def tkShow(self):
        self.printClueInfo()
        '''
        Main graphical grid filling method.
        Puts the puzzle fill into the constituent Tk labels'''
        i = 0
        labelfont = ('arial', 15)
        for line in self.puzzle.fill:
            j = 0
            if not self.alive:
                self.labels.append([])
            for char in line:
                if char == '-': theChar = ' '
                else: theChar = char
                #highlight incorrect fill
                correctCh = self.puzzle.solution[i][j]
                if theChar == '.':
                    color = 'black'
                elif theChar.upper() == correctCh.upper():
                    color = 'white'
                else:
                    color = 'lightsalmon'
                if not self.alive:
                    '''Spawn label on first run'''
                    l = Label(self.root, text = ' %s ' % (theChar), relief = SOLID, font = labelfont, width = 2, bg = color)
                    self.labels[i].append(l)
                    if self.puzzle.parent and self.puzzle.fill[i][j] != self.puzzle.parent.fill[i][j]:
                        self.labels[i][j].config(relief = SUNKEN)
                    self.labels[i][j].grid(row=i, column=j, sticky=NSEW)
                else:
                    '''Update existing label if mainloop is already running'''
                    if self.puzzle.parent and self.puzzle.fill[i][j] != self.puzzle.parent.fill[i][j]:
                        self.labels[i][j].config(relief = SUNKEN)
                    else:
                        self.labels[i][j].config(relief = SOLID)
                    self.labels[i][j].config(text = ' %s ' % (theChar),  bg = color)
                j += 1
            i += 1
        if not self.alive:
            #populate UI
            self.drawInterface()
            self.checkButtons()
            '''Launch main Tk window'''
            self.alive = True
            self.root.configure(background = 'grey20')
            self.printClueInfo()
            self.root.mainloop()
        else:
            '''Force root window to update its constituents'''
            self.checkButtons()
            self.root.update_idletasks()
        return

    def checkButtons(self):
        '''Disables navigation buttons as appropriate'''
        if not self.puzzle.parent:
            self.prevButton.config(state = DISABLED)
        else:
            self.prevButton.config(state = NORMAL)
        if len(self.trace) == 0:
            self.nextButton.config(state = DISABLED)
        else:
            self.nextButton.config(state = NORMAL)

    def previous(self):
        '''Button and arrow key handler; also advances pointer'''
        if self.puzzle.parent:
            self.trace.append(self.puzzle)
            #print len(self.trace)
            self.puzzle = self.puzzle.parent
            self.tkShow()

    def next(self):
        '''Button and arrow key handler; also retreats pointer'''
        if len(self.trace) < 1:
            return
        self.puzzle = self.trace.pop(len(self.trace) - 1)
        #print len(self.trace)
        self.tkShow()

    def animate(self):
        '''Plays the trace frame by frame'''
        while self.puzzle.parent:
            self.trace.append(self.puzzle)
            self.puzzle = self.puzzle.parent
        while len(self.trace) > 0:
            self.puzzle = self.trace.pop(len(self.trace) - 1)
            self.tkShow()
            time.sleep(0.1)

    def select(self, evt):
        '''Listbox select callback (underlines selected clue and updates main Label'''
        w = evt.widget
        index = int(w.curselection()[0])
        clueKey = w.get(index).split()[0]
        clue = self.puzzle.clueDict[clueKey]
        # clear the background of the previously selected
        if self.currSel:
            currClue = self.puzzle.clueDict[self.currSel]
            for i in range(len(currClue)):
                if 'A' in self.currSel:
                    if self.puzzle.fill[currClue.row][currClue.col + i] == self.puzzle.solution[currClue.row][currClue.col + i]:
                        self.labels[currClue.row][currClue.col + i].config(bg = 'white')
                    else:
                        self.labels[currClue.row][currClue.col + i].config(bg = 'lightsalmon')
                if 'D' in self.currSel:
                    if self.puzzle.fill[currClue.row + i][currClue.col] == self.puzzle.solution[currClue.row + i][currClue.col]:
                        self.labels[currClue.row + i][currClue.col].config(bg = 'white')
                    else:
                        self.labels[currClue.row + i][currClue.col].config(bg = 'lightsalmon')
        #record and color currently selected clue
        self.currSel = clueKey
        for i in range(len(clue)):
            if 'A' in clueKey:
                if self.puzzle.fill[clue.row][clue.col + i] == self.puzzle.solution[clue.row][clue.col + i]:
                    self.labels[clue.row][clue.col + i].config(bg = 'light sky blue')
                else:
                    self.labels[clue.row][clue.col + i].config(bg = 'orchid')
            if 'D' in clueKey:
                if self.puzzle.fill[clue.row + i][clue.col] == self.puzzle.solution[clue.row + i][clue.col]:
                    self.labels[clue.row + i][clue.col].config(bg = 'light sky blue')
                else:
                    self.labels[clue.row + i][clue.col].config(bg = 'orchid')
        self.printClueInfo()
        #force update
        self.root.update_idletasks()

    def printClueInfo(self):
        '''Fills large Label with relevant info about the currently selected clue'''
        #display current fill and clue solution
        if self.currSel:
            f = self.puzzle.getClueFill(self.currSel)
            s = self.puzzle.getClueFill(self.currSel, True)
            content = 'Clue ' + self.currSel + ':\n' + self.puzzle.clueDict[self.currSel].clue + '\nCurrent puzzle fill:\t' + f + '\nSolution:\t\t' + s
            content += '\n\n Score: ' + str(self.puzzle.score()[0]) + '/' + str(self.puzzle.score()[1]) + ' correct'
            self.clueInfo.config(text = content)

    def keyPress(self, evt):
        '''Keyboard event handler'''
        if evt.keycode == 37 and self.puzzle.parent:
            self.previous()
            return
        if evt.keycode == 39 and len(self.trace) > 0:
            self.next()
